﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;
using HarmonicUnity;

public class Direction4Test
{
    [Test]
    public void FromVector2Test() {
        Assert.AreEqual(Direction4.Down, Direction4Utils.FromVector2(new Vector2(0.0f, -1.0f)));
        Assert.AreEqual(Direction4.Left, Direction4Utils.FromVector2(new Vector2(-1.0f, 0.0f)));
        Assert.AreEqual(Direction4.Right, Direction4Utils.FromVector2(new Vector2(1.0f, 0.0f)));
        Assert.AreEqual(Direction4.Up, Direction4Utils.FromVector2(new Vector2(0.0f, 1.0f)));
    }

    [Test]
    public void ToVector2Test() {
        Assert.AreEqual(new Vector2(0.0f, -1.0f), Direction4.Down.ToVector2());
        Assert.AreEqual(new Vector2(-1.0f, 0.0f), Direction4.Left.ToVector2());
        Assert.AreEqual(new Vector2(1.0f, 0.0f), Direction4.Right.ToVector2());
        Assert.AreEqual(new Vector2(0.0f, 1.0f), Direction4.Up.ToVector2());
    }

    [Test]
    public void OppositeTest() {
        Assert.AreEqual(Direction4.Up, Direction4.Down.Opposite());
        Assert.AreEqual(Direction4.Right, Direction4.Left.Opposite());
        Assert.AreEqual(Direction4.Left, Direction4.Right.Opposite());
        Assert.AreEqual(Direction4.Down, Direction4.Up.Opposite());
    }
}
