﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;
using HarmonicUnity;

public class TransformExtensionTest
{
    [Test]
    public void SetPositionTest() {
        Transform transform = new GameObject().transform;
        transform.SetPositionX(10);
        transform.SetPositionY(20);
        transform.SetPositionZ(30);
        Assert.AreEqual(new Vector3(10, 20, 30), transform.position);
    }

    [Test]
    public void SetLocalPositionTest() {
        Transform transform = new GameObject().transform;
        transform.SetLocalPositionX(10);
        transform.SetLocalPositionY(20);
        transform.SetLocalPositionZ(30);
        Assert.AreEqual(new Vector3(10, 20, 30), transform.localPosition);
    }

    [Test]
    public void SetLocalScaleTest() {
        Transform transform = new GameObject().transform;
        transform.SetLocalScaleX(10);
        transform.SetLocalScaleY(20);
        transform.SetLocalScaleZ(30);
        Assert.AreEqual(new Vector3(10, 20, 30), transform.localScale);
    }
}
