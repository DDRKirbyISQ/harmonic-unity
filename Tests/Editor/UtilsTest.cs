﻿using NUnit.Framework;
using HarmonicUnity;

public class UtilsTest
{
    [Test]
    public void ModPositiveTest() {
        Assert.AreEqual(2.0f, Utils.ModPositive(2.0f, 3.0f));
        Assert.AreEqual(1.0f, Utils.ModPositive(4.0f, 3.0f));
        Assert.AreEqual(1.0f, Utils.ModPositive(-2.0f, 3.0f));
        Assert.AreEqual(2.0f, Utils.ModPositive(-4.0f, 3.0f));
        Assert.AreEqual(0.0f, Utils.ModPositive(0.0f, 3.0f));

        Assert.AreEqual(2.0f, Utils.ModPositive(2.0f, -3.0f));
        Assert.AreEqual(1.0f, Utils.ModPositive(4.0f, -3.0f));
        Assert.AreEqual(1.0f, Utils.ModPositive(-2.0f, -3.0f));
        Assert.AreEqual(2.0f, Utils.ModPositive(-4.0f, -3.0f));
        Assert.AreEqual(0.0f, Utils.ModPositive(0.0f, -3.0f));
    }

    [Test]
    public void ModPositiveIntTest() {
        Assert.AreEqual(2, Utils.ModPositive(2, 3));
        Assert.AreEqual(1, Utils.ModPositive(4, 3));
        Assert.AreEqual(1, Utils.ModPositive(-2, 3));
        Assert.AreEqual(2, Utils.ModPositive(-4, 3));
        Assert.AreEqual(0, Utils.ModPositive(0, 3));

        Assert.AreEqual(2, Utils.ModPositive(2, -3));
        Assert.AreEqual(1, Utils.ModPositive(4, -3));
        Assert.AreEqual(1, Utils.ModPositive(-2, -3));
        Assert.AreEqual(2, Utils.ModPositive(-4, -3));
        Assert.AreEqual(0, Utils.ModPositive(0, -3));
    }
}
