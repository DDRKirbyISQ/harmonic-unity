﻿// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

Shader "HarmonicUnity/HSVASprite-instanced"
{
    Properties
    {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
        _Color ("Tint", Color) = (1,1,1,1)
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 1
        [HideInInspector] _RendererColor ("RendererColor", Color) = (1,1,1,1)
        [HideInInspector] _Flip ("Flip", Vector) = (1,1,1,1)
        [PerRendererData] _HSVAAdjust ("HSVA Adjust", Vector) = (0, 0, 0, 0)
        [PerRendererData] [MaterialToggle] _Invert ("Invert", Float) = 0
    }

    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

        Cull Off
        Lighting Off
        ZWrite Off
        Fog { Mode Off }
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
        CGPROGRAM
            #pragma vertex FixedSpriteVert
            #pragma fragment frag
            #pragma target 2.0
            #pragma multi_compile_instancing
            #pragma multi_compile_local _ PIXELSNAP_ON
            #include "UnitySprites.cginc"

            // Fixed PixelSnap, handles odd screen resolutions.
            inline float4 FixedPixelSnap (float4 pos)
            {
                float2 hpc = _ScreenParams.xy * 0.5;
                if (_ScreenParams.x % 2 > 0.5) {
                    hpc.x = _ScreenParams.x;
                }
                if (_ScreenParams.y % 2 > 0.5) {
                    hpc.y = _ScreenParams.y;
                }

                float2 pixelPos = (pos.xy / pos.w) * hpc;
                pixelPos = round(pixelPos);
                pos.xy = pixelPos / hpc * pos.w;
                return pos;
            }

            v2f FixedSpriteVert(appdata_t IN)
            {
                v2f OUT;

                UNITY_SETUP_INSTANCE_ID (IN);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);

                OUT.vertex = UnityFlipSprite(IN.vertex, _Flip);
                OUT.vertex = UnityObjectToClipPos(OUT.vertex);
                OUT.texcoord = IN.texcoord;
                OUT.color = IN.color * _Color * _RendererColor;

                #ifdef PIXELSNAP_ON
                OUT.vertex = FixedPixelSnap (OUT.vertex);
                #endif

                return OUT;
            }
            
            fixed4 _HSVAAdjust;
            fixed _Invert;

            fixed3 RGBtoHCV(in fixed3 RGB)
            {
                // Based on work by Sam Hocevar and Emil Persson
                fixed4 P = (RGB.g < RGB.b) ? fixed4(RGB.bg, -1.0, 2.0/3.0) : fixed4(RGB.gb, 0.0, -1.0/3.0);
                fixed4 Q = (RGB.r < P.x) ? fixed4(P.xyw, RGB.r) : fixed4(RGB.r, P.yzx);
                fixed C = Q.x - min(Q.w, Q.y);
                fixed H = C == 0 ? 1 : abs((Q.w - Q.y) / (6.0 * C) + Q.z);
                return fixed3(H, C, Q.x);
            }

            fixed3 RGBtoHSV(in fixed3 RGB)
            {
                fixed3 HCV = RGBtoHCV(RGB);
                fixed S = HCV.z == 0 ? 1 : HCV.y / HCV.z;
                return fixed3(HCV.x, S, HCV.z);
            }

            fixed3 HUEtoRGB(in fixed H)
            {
                fixed R = abs(H * 6 - 3) - 1;
                fixed G = 2 - abs(H * 6 - 2);
                fixed B = 2 - abs(H * 6 - 4);
                return saturate(fixed3(R,G,B));
            }

            fixed3 HSVtoRGB(in fixed3 HSV)
            {
                fixed3 RGB = HUEtoRGB(HSV.x);
                return ((RGB - 1) * HSV.y + 1) * HSV.z;
            }

            fixed4 frag(v2f IN) : SV_Target
            {
                fixed4 texcol = tex2D (_MainTex, IN.texcoord);
                texcol.rgb = lerp(fixed3(1.0, 1.0, 1.0) - texcol.rgb, texcol.rgb, step(_Invert, 0.5));
                fixed3 hsv = RGBtoHSV(texcol.rgb) + _HSVAAdjust.xyz;
                hsv.x = frac(hsv.x);
                fixed3 rgb = HSVtoRGB(hsv);
                return fixed4(rgb, texcol.a + _HSVAAdjust.a) * IN.color;
            }

        ENDCG
        }
    }

    Fallback "Sprites/Default"
}
