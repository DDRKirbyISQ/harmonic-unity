﻿Shader "HarmonicUnity/Sprites-Additive"
{
    Properties
    {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
        _Color ("Tint", Color) = (1,1,1,1)
        [PerRendererData] _Alpha ("Alpha", Float) = 1
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
        [HideInInspector] _RendererColor ("RendererColor", Color) = (1,1,1,1)
        [HideInInspector] _Flip ("Flip", Vector) = (1,1,1,1)
        [PerRendererData] _AlphaTex ("External Alpha", 2D) = "white" {}
        [PerRendererData] _EnableExternalAlpha ("Enable External Alpha", Float) = 0
    }

    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

        Cull Off
        Lighting Off
        ZWrite Off
        Blend SrcAlpha One

        Pass
        {
        CGPROGRAM
            #pragma vertex FixedSpriteVert
            #pragma fragment SpriteFrag
            #pragma target 2.0
            #pragma multi_compile_instancing
            #pragma multi_compile_local _ PIXELSNAP_ON
            #pragma multi_compile _ ETC1_EXTERNAL_ALPHA
            #include "UnitySprites.cginc"

            fixed _Alpha;

            // Fixed PixelSnap, handles odd screen resolutions.
            inline float4 FixedPixelSnap (float4 pos)
            {
                float2 hpc = _ScreenParams.xy * 0.5;
                if (_ScreenParams.x % 2 > 0.5) {
                    hpc.x = _ScreenParams.x;
                }
                if (_ScreenParams.y % 2 > 0.5) {
                    hpc.y = _ScreenParams.y;
                }

                float2 pixelPos = (pos.xy / pos.w) * hpc;
                pixelPos = round(pixelPos);
                pos.xy = pixelPos / hpc * pos.w;
                return pos;
            }

            v2f FixedSpriteVert(appdata_t IN)
            {
                v2f OUT;

                UNITY_SETUP_INSTANCE_ID (IN);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);

                OUT.vertex = UnityFlipSprite(IN.vertex, _Flip);
                OUT.vertex = UnityObjectToClipPos(OUT.vertex);
                OUT.texcoord = IN.texcoord;
                OUT.color = IN.color * _Color * _RendererColor;
                OUT.color.a *= _Alpha;

                #ifdef PIXELSNAP_ON
                OUT.vertex = FixedPixelSnap (OUT.vertex);
                #endif

                return OUT;
            }

        ENDCG
        }
    }
}
