// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

    Shader "TextMeshPro/BitmapShadow" {
     
    Properties {
        _MainTex        ("Font Atlas", 2D) = "white" {}
        _FaceTex        ("Font Texture", 2D) = "white" {}
        _FaceColor        ("Text Color", Color) = (1,1,1,1)
     
        _ShadowColor    ("Shadow Color", Color) = (0,0,0,0)
        _Shadow1X        ("Shadow 1X", Float) = 0
        _Shadow1Y        ("Shadow 1Y", Float) = 0
        _Shadow2X        ("Shadow 2X", Float) = 0
        _Shadow2Y        ("Shadow 2Y", Float) = 0
        _Shadow3X        ("Shadow 1X", Float) = 0
        _Shadow3Y        ("Shadow 1Y", Float) = 0
        _Shadow4X        ("Shadow 2X", Float) = 0
        _Shadow4Y        ("Shadow 2Y", Float) = 0
     
        _StencilComp("Stencil Comparison", Float) = 8
        _Stencil("Stencil ID", Float) = 0
        _StencilOp("Stencil Operation", Float) = 0
        _StencilWriteMask("Stencil Write Mask", Float) = 255
        _StencilReadMask("Stencil Read Mask", Float) = 255
     
    	_CullMode("Cull Mode", Float) = 0
        _ColorMask("Color Mask", Float) = 15
    }
     
    SubShader{
     
        Tags { "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
     
        Stencil
        {
            Ref[_Stencil]
            Comp[_StencilComp]
            Pass[_StencilOp]
            ReadMask[_StencilReadMask]
            WriteMask[_StencilWriteMask]
        }
     
     
        Lighting Off
        Cull [_CullMode]
        ZTest [unity_GUIZTestMode]
        ZWrite Off
        Fog { Mode Off }
        Blend SrcAlpha OneMinusSrcAlpha
        ColorMask[_ColorMask]
     
     
        Pass {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
     
            #include "UnityCG.cginc"
     
     
            struct appdata_t {
                float4 vertex        : POSITION;
                fixed4 color        : COLOR;
                float2 texcoord0    : TEXCOORD0;
                float2 texcoord1    : TEXCOORD1;
            };
     
            struct v2f {
                float4    vertex        : POSITION;
                fixed4    color        : COLOR;
                float2    texcoord0    : TEXCOORD0;
                float2    texcoord1    : TEXCOORD1;
            };
     
            uniform    sampler2D     _MainTex;
            uniform    sampler2D     _FaceTex;
            uniform float4        _FaceTex_ST;
            uniform    fixed4        _FaceColor;
     
            uniform    fixed4        _ShadowColor;
            uniform float        _Shadow1X;
            uniform float        _Shadow1Y;
            uniform float        _Shadow2X;
            uniform float        _Shadow2Y;
            uniform float        _Shadow3X;
            uniform float        _Shadow3Y;
            uniform float        _Shadow4X;
            uniform float        _Shadow4Y;
     
            float2 UnpackUV(float uv)
            {
                float2 output;
                output.x = floor(uv / 4096);
                output.y = uv - 4096 * output.x;
     
                return output * 0.001953125;
            }
     
            // Fixed PixelSnap, use ceil instead of round, fixes artifacts for some reason.
            inline float4 CeilPixelSnap (float4 pos)
            {
                float2 hpc = _ScreenParams.xy * 2;
                float2 pixelPos = ceil ((pos.xy / pos.w) * hpc);
                pos.xy = pixelPos / hpc * pos.w;
                return pos;
            }
     
            v2f vert (appdata_t i)
            {
                float4 vert = i.vertex;
                vert.x += _Shadow1X;
                vert.y += _Shadow1Y;
     
                vert.xy += (vert.w * 0.5) / _ScreenParams.xy;
     
                float4 vPosition = CeilPixelSnap(UnityObjectToClipPos(vert));
     
                fixed4 faceColor = i.color;
                faceColor *= _ShadowColor;
     
                v2f o;
                o.vertex = vPosition;
                o.color = faceColor;
                o.texcoord0 = i.texcoord0;
                o.texcoord1 = TRANSFORM_TEX(UnpackUV(i.texcoord1), _FaceTex);
                float2 pixelSize = vPosition.w;
                pixelSize /= abs(float2(_ScreenParams.x * UNITY_MATRIX_P[0][0], _ScreenParams.y * UNITY_MATRIX_P[1][1]));
     
                return o;
            }
     
            fixed4 frag (v2f i) : COLOR
            {
                //fixed4 c = tex2D(_MainTex, i.texcoord0) * tex2D(_FaceTex, i.texcoord1) * i.color;
             
                fixed4 c = tex2D(_MainTex, i.texcoord0);
                c = fixed4 (tex2D(_FaceTex, i.texcoord1).rgb * i.color.rgb, i.color.a * c.a);
     
                return c;
            }
            ENDCG
        }
     
     
        Pass {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
     
            #include "UnityCG.cginc"
     
     
        #if UNITY_VERSION < 530
            bool _UseClipRect;
        #endif
     
            struct appdata_t {
                float4 vertex        : POSITION;
                fixed4 color        : COLOR;
                float2 texcoord0    : TEXCOORD0;
                float2 texcoord1    : TEXCOORD1;
            };
     
            struct v2f {
                float4    vertex        : POSITION;
                fixed4    color        : COLOR;
                float2    texcoord0    : TEXCOORD0;
                float2    texcoord1    : TEXCOORD1;
            };
     
            uniform    sampler2D     _MainTex;
            uniform    sampler2D     _FaceTex;
            uniform float4        _FaceTex_ST;
            uniform    fixed4        _FaceColor;
     
            uniform    fixed4        _ShadowColor;
            uniform float        _Shadow1X;
            uniform float        _Shadow1Y;
            uniform float        _Shadow2X;
            uniform float        _Shadow2Y;
            uniform float        _Shadow3X;
            uniform float        _Shadow3Y;
            uniform float        _Shadow4X;
            uniform float        _Shadow4Y;
     
            uniform float4        _ClipRect;
     
            float2 UnpackUV(float uv)
            {
                float2 output;
                output.x = floor(uv / 4096);
                output.y = uv - 4096 * output.x;
     
                return output * 0.001953125;
            }
     
            // Fixed PixelSnap, use ceil instead of round, fixes artifacts for some reason.
            inline float4 CeilPixelSnap (float4 pos)
            {
                float2 hpc = _ScreenParams.xy * 2;
                float2 pixelPos = ceil ((pos.xy / pos.w) * hpc);
                pos.xy = pixelPos / hpc * pos.w;
                return pos;
            }
     
            v2f vert (appdata_t i)
            {
                float4 vert = i.vertex;
                vert.x += _Shadow2X;
                vert.y += _Shadow2Y;
     
                vert.xy += (vert.w * 0.5) / _ScreenParams.xy;
     
                float4 vPosition = CeilPixelSnap(UnityObjectToClipPos(vert));
     
                fixed4 faceColor = i.color;
                faceColor *= _ShadowColor;
     
                v2f o;
                o.vertex = vPosition;
                o.color = faceColor;
                o.texcoord0 = i.texcoord0;
                o.texcoord1 = TRANSFORM_TEX(UnpackUV(i.texcoord1), _FaceTex);
                float2 pixelSize = vPosition.w;
                pixelSize /= abs(float2(_ScreenParams.x * UNITY_MATRIX_P[0][0], _ScreenParams.y * UNITY_MATRIX_P[1][1]));
     
                return o;
            }
     
            fixed4 frag (v2f i) : COLOR
            {
                //fixed4 c = tex2D(_MainTex, i.texcoord0) * tex2D(_FaceTex, i.texcoord1) * i.color;
             
                fixed4 c = tex2D(_MainTex, i.texcoord0);
                c = fixed4 (tex2D(_FaceTex, i.texcoord1).rgb * i.color.rgb, i.color.a * c.a);
     
                return c;
            }
            ENDCG
        }
     
     
     
        Pass {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
     
            #include "UnityCG.cginc"
     
     
        #if UNITY_VERSION < 530
            bool _UseClipRect;
        #endif
     
            struct appdata_t {
                float4 vertex        : POSITION;
                fixed4 color        : COLOR;
                float2 texcoord0    : TEXCOORD0;
                float2 texcoord1    : TEXCOORD1;
            };
     
            struct v2f {
                float4    vertex        : POSITION;
                fixed4    color        : COLOR;
                float2    texcoord0    : TEXCOORD0;
                float2    texcoord1    : TEXCOORD1;
            };
     
            uniform    sampler2D     _MainTex;
            uniform    sampler2D     _FaceTex;
            uniform float4        _FaceTex_ST;
            uniform    fixed4        _FaceColor;
     
            uniform    fixed4        _ShadowColor;
            uniform float        _Shadow1X;
            uniform float        _Shadow1Y;
            uniform float        _Shadow2X;
            uniform float        _Shadow2Y;
            uniform float        _Shadow3X;
            uniform float        _Shadow3Y;
            uniform float        _Shadow4X;
            uniform float        _Shadow4Y;
     
            uniform float4        _ClipRect;
     
            float2 UnpackUV(float uv)
            {
                float2 output;
                output.x = floor(uv / 4096);
                output.y = uv - 4096 * output.x;
     
                return output * 0.001953125;
            }
     
            // Fixed PixelSnap, use ceil instead of round, fixes artifacts for some reason.
            inline float4 CeilPixelSnap (float4 pos)
            {
                float2 hpc = _ScreenParams.xy * 2;
                float2 pixelPos = ceil ((pos.xy / pos.w) * hpc);
                pos.xy = pixelPos / hpc * pos.w;
                return pos;
            }
     
            v2f vert (appdata_t i)
            {
                float4 vert = i.vertex;
                vert.x += _Shadow3X;
                vert.y += _Shadow3Y;
     
                vert.xy += (vert.w * 0.5) / _ScreenParams.xy;
     
                float4 vPosition = CeilPixelSnap(UnityObjectToClipPos(vert));
     
                fixed4 faceColor = i.color;
                faceColor *= _ShadowColor;
     
                v2f o;
                o.vertex = vPosition;
                o.color = faceColor;
                o.texcoord0 = i.texcoord0;
                o.texcoord1 = TRANSFORM_TEX(UnpackUV(i.texcoord1), _FaceTex);
                float2 pixelSize = vPosition.w;
                pixelSize /= abs(float2(_ScreenParams.x * UNITY_MATRIX_P[0][0], _ScreenParams.y * UNITY_MATRIX_P[1][1]));
     
                return o;
            }
     
            fixed4 frag (v2f i) : COLOR
            {
                //fixed4 c = tex2D(_MainTex, i.texcoord0) * tex2D(_FaceTex, i.texcoord1) * i.color;
             
                fixed4 c = tex2D(_MainTex, i.texcoord0);
                c = fixed4 (tex2D(_FaceTex, i.texcoord1).rgb * i.color.rgb, i.color.a * c.a);
     
                return c;
            }
            ENDCG
        }
     
     
     
        Pass {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
     
            #include "UnityCG.cginc"
     
     
        #if UNITY_VERSION < 530
            bool _UseClipRect;
        #endif
     
            struct appdata_t {
                float4 vertex        : POSITION;
                fixed4 color        : COLOR;
                float2 texcoord0    : TEXCOORD0;
                float2 texcoord1    : TEXCOORD1;
            };
     
            struct v2f {
                float4    vertex        : POSITION;
                fixed4    color        : COLOR;
                float2    texcoord0    : TEXCOORD0;
                float2    texcoord1    : TEXCOORD1;
            };
     
            uniform    sampler2D     _MainTex;
            uniform    sampler2D     _FaceTex;
            uniform float4        _FaceTex_ST;
            uniform    fixed4        _FaceColor;
     
            uniform    fixed4        _ShadowColor;
            uniform float        _Shadow1X;
            uniform float        _Shadow1Y;
            uniform float        _Shadow2X;
            uniform float        _Shadow2Y;
            uniform float        _Shadow3X;
            uniform float        _Shadow3Y;
            uniform float        _Shadow4X;
            uniform float        _Shadow4Y;
     
            uniform float4        _ClipRect;
     
            float2 UnpackUV(float uv)
            {
                float2 output;
                output.x = floor(uv / 4096);
                output.y = uv - 4096 * output.x;
     
                return output * 0.001953125;
            }
     
            // Fixed PixelSnap, use ceil instead of round, fixes artifacts for some reason.
            inline float4 CeilPixelSnap (float4 pos)
            {
                float2 hpc = _ScreenParams.xy * 2;
                float2 pixelPos = ceil ((pos.xy / pos.w) * hpc);
                pos.xy = pixelPos / hpc * pos.w;
                return pos;
            }
     
            v2f vert (appdata_t i)
            {
                float4 vert = i.vertex;
                vert.x += _Shadow4X;
                vert.y += _Shadow4Y;
     
                vert.xy += (vert.w * 0.5) / _ScreenParams.xy;
     
                float4 vPosition = CeilPixelSnap(UnityObjectToClipPos(vert));
     
                fixed4 faceColor = i.color;
                faceColor *= _ShadowColor;
     
                v2f o;
                o.vertex = vPosition;
                o.color = faceColor;
                o.texcoord0 = i.texcoord0;
                o.texcoord1 = TRANSFORM_TEX(UnpackUV(i.texcoord1), _FaceTex);
                float2 pixelSize = vPosition.w;
                pixelSize /= abs(float2(_ScreenParams.x * UNITY_MATRIX_P[0][0], _ScreenParams.y * UNITY_MATRIX_P[1][1]));
     
                return o;
            }
     
            fixed4 frag (v2f i) : COLOR
            {
                //fixed4 c = tex2D(_MainTex, i.texcoord0) * tex2D(_FaceTex, i.texcoord1) * i.color;
             
                fixed4 c = tex2D(_MainTex, i.texcoord0);
                c = fixed4 (tex2D(_FaceTex, i.texcoord1).rgb * i.color.rgb, i.color.a * c.a);
     
                return c;
            }
            ENDCG
        }
     
     
        Pass {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
     
            #include "UnityCG.cginc"
     
     
        #if UNITY_VERSION < 530
            bool _UseClipRect;
        #endif
     
            struct appdata_t {
                float4 vertex        : POSITION;
                fixed4 color        : COLOR;
                float2 texcoord0    : TEXCOORD0;
                float2 texcoord1    : TEXCOORD1;
            };
     
            struct v2f {
                float4    vertex        : POSITION;
                fixed4    color        : COLOR;
                float2    texcoord0    : TEXCOORD0;
                float2    texcoord1    : TEXCOORD1;
            };
     
            uniform    sampler2D     _MainTex;
            uniform    sampler2D     _FaceTex;
            uniform float4        _FaceTex_ST;
            uniform    fixed4        _FaceColor;
     
            uniform float4        _ClipRect;
     
            float2 UnpackUV(float uv)
            {
                float2 output;
                output.x = floor(uv / 4096);
                output.y = uv - 4096 * output.x;
     
                return output * 0.001953125;
            }

            // Fixed PixelSnap, use ceil instead of round, fixes artifacts for some reason.
            inline float4 CeilPixelSnap (float4 pos)
            {
                float2 hpc = _ScreenParams.xy * 2;
                float2 pixelPos = ceil ((pos.xy / pos.w) * hpc);
                pos.xy = pixelPos / hpc * pos.w;
                return pos;
            }
     
            v2f vert (appdata_t i)
            {
                float4 vert = i.vertex;
     
                vert.xy += (vert.w * 0.5) / _ScreenParams.xy;
     
                float4 vPosition = CeilPixelSnap(UnityObjectToClipPos(vert));
     
                fixed4 faceColor = i.color;
                faceColor *= _FaceColor;
     
                v2f o;
                o.vertex = vPosition;
                o.color = faceColor;
                o.texcoord0 = i.texcoord0;
                o.texcoord1 = TRANSFORM_TEX(UnpackUV(i.texcoord1), _FaceTex);
                float2 pixelSize = vPosition.w;
                pixelSize /= abs(float2(_ScreenParams.x * UNITY_MATRIX_P[0][0], _ScreenParams.y * UNITY_MATRIX_P[1][1]));
     
                return o;
            }
     
            fixed4 frag (v2f i) : COLOR
            {
                //fixed4 c = tex2D(_MainTex, i.texcoord0) * tex2D(_FaceTex, i.texcoord1) * i.color;
             
                fixed4 c = tex2D(_MainTex, i.texcoord0);
                c = fixed4 (tex2D(_FaceTex, i.texcoord1).rgb * i.color.rgb, i.color.a * c.a);
     
                return c;
            }
            ENDCG
        }
    }
     
    }
     
