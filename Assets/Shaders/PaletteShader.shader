Shader "HarmonicUnity/PaletteShader"
{
    Properties
    {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
        _PaletteTex ("Palette Texture", 2D) = "" {}
        _Color ("Tint", Color) = (1,1,1,1)
        [PerRendererData] _Alpha ("Alpha", Float) = 1
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 1
        [HideInInspector] _RendererColor ("RendererColor", Color) = (1,1,1,1)
        [HideInInspector] _Flip ("Flip", Vector) = (1,1,1,1)
        _PaletteIndex ("Palette Index", Float) = 0
        [PerRendererData] _IsBackground ("Is Background", Float) = 0
        _ScreenFilter ("Screen Filter", Float) = 0
    }

    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

        Cull Off
        Lighting Off
        ZWrite Off
        Fog { Mode Off }
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
        CGPROGRAM
            #pragma vertex FixedSpriteVert
            #pragma fragment frag
            #pragma target 2.0
            #pragma multi_compile_instancing
            #pragma multi_compile_local _ PIXELSNAP_ON
            #include "UnitySprites.cginc"

            sampler2D _PaletteTex;
            float4 _PaletteTex_TexelSize;
            fixed _PaletteIndex;
            fixed _Alpha;
            fixed _IsBackground;
            fixed _ScreenFilter;

            // Fixed PixelSnap, handles odd screen resolutions.
            inline float4 FixedPixelSnap (float4 pos)
            {
                float2 hpc = _ScreenParams.xy * 0.5;
                if (_ScreenParams.x % 2 > 0.5) {
                    hpc.x = _ScreenParams.x;
                }
                if (_ScreenParams.y % 2 > 0.5) {
                    hpc.y = _ScreenParams.y;
                }

                float2 pixelPos = (pos.xy / pos.w) * hpc;
                pixelPos = round(pixelPos);
                pos.xy = pixelPos / hpc * pos.w;
                return pos;
            }

            v2f FixedSpriteVert(appdata_t IN)
            {
                v2f OUT;

                UNITY_SETUP_INSTANCE_ID (IN);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);

                OUT.vertex = UnityFlipSprite(IN.vertex, _Flip);
                OUT.vertex = UnityObjectToClipPos(OUT.vertex);
                OUT.texcoord = IN.texcoord;
                OUT.color = IN.color * _Color * _RendererColor;
                OUT.color.a *= _Alpha;

                #ifdef PIXELSNAP_ON
                OUT.vertex = FixedPixelSnap (OUT.vertex);
                #endif

                return OUT;
            }

            fixed4 frag(v2f IN) : SV_Target
            {
                fixed4 texcol = SampleSpriteTexture(IN.texcoord);
                fixed average = (texcol.r + texcol.g + texcol.b) * 0.333333333333333;
                float x = average * _PaletteTex_TexelSize.x * (_PaletteTex_TexelSize.z - 1) + 0.5 * _PaletteTex_TexelSize.x;
                float y = 1.0 - (_PaletteTex_TexelSize.y * (_PaletteIndex + 0.5));
                texcol.rgb = tex2D(_PaletteTex, half2(x, y));

                // Background color is second to last in the palette
                float backgroundX = 0.90196078431372549019607843137255 * _PaletteTex_TexelSize.x * (_PaletteTex_TexelSize.z - 1) + 0.5 * _PaletteTex_TexelSize.x;
                fixed4 background = tex2D(_PaletteTex, half2(backgroundX, y));

                if (_IsBackground > 0.5) {
                    if (_ScreenFilter < 0.5) {
                        // 0 = screenfilter off.
                        return texcol * IN.color;
                    } else if (_ScreenFilter < 1.5) {
                        // 1 = mix with background color.
                        texcol.rgb = (texcol.rgb + background.rgb) * 0.5;
                        return texcol * IN.color;
                    } else if (_ScreenFilter < 2.5) {
                        // 2 = darken.
                        texcol.rgb = texcol.rgb * 0.5;
                        return texcol * IN.color;
                    } else {
                        // 3 = desaturate.
                        fixed luma = 0.3*texcol.r + 0.6*texcol.g + 0.1*texcol.b;
                        texcol.r = texcol.r + 0.5 * (luma - texcol.r);
                        texcol.g = texcol.g + 0.5 * (luma - texcol.g);
                        texcol.b = texcol.b + 0.5 * (luma - texcol.b);
                        return texcol * IN.color;
                    }
                }
                return texcol * IN.color;
            }

        ENDCG
        }
    }

    Fallback "Sprites/Default"
}
