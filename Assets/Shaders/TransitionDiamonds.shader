﻿Shader "HarmonicUnity/Transition-Diamonds"
{
    Properties
    {
        _Progress ("Progress", Range(0.0, 1.0)) = 0.5
        [MaterialToggle] _Reverse ("Reverse", Float) = 0.0
        _Pixelate ("Pixelate", Range(1.0, 4.0)) = 1.0
        _Diamonds ("Diamond Count (max row/col)", Float) = 20
        [PerRendererData] _MainTex ("Main Texture", 2D) = "white" {}
    }

    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

        Cull Off
        Lighting Off
        ZWrite Off
        Fog { Mode Off }
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
        CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            float _Progress;
            float _Pixelate;
            float _Reverse;
            float _Diamonds;

            // vertex shader inputs
            struct appdata
            {
                float4 vertex : POSITION; // vertex position
                float2 uv : TEXCOORD0; // texture coordinate
                float4 color : COLOR;
            };
            
            // vertex shader outputs ("vertex to fragment")
            struct v2f
            {
                float2 uv : TEXCOORD0; // texture coordinate
                float4 vertex : SV_POSITION; // clip space position
                float4 color : COLOR;
            };

            // vertex shader
            v2f vert (appdata v)
            {
                v2f o;
                // transform position to clip space
                // (multiply with model*view*projection matrix)
                o.vertex = UnityObjectToClipPos(v.vertex);
                // just pass the texture coordinate
                o.uv = v.uv;
                o.color = v.color;
                return o;
            }            

            fixed4 frag(v2f IN) : SV_Target
            {
                float div = max(_ScreenParams.x, _ScreenParams.y);
                float inX = IN.vertex.x;
                float inY = IN.vertex.y;
                inX = floor(inX / _Pixelate + 0.5) * _Pixelate;
                inY = floor(inY / _Pixelate + 0.5) * _Pixelate;
                float xFraction = frac(inX / div * _Diamonds);
                float yFraction = frac(inY / div * _Diamonds);
                float xDistance = abs(xFraction - 0.5) * 2;
                float yDistance = abs(yFraction - 0.5) * 2;
                float x = IN.uv.x;
                float y = 1 - IN.uv.y;

                if (_Reverse > 0.5) {
                    x = 1 - x;
                    y = 1 - y;
                }
                if (xDistance + yDistance + x + y > _Progress * 4) {
                    discard;
                }
                return IN.color;
            }

        ENDCG
        }
    }

    Fallback "Sprites/Default"
}
