﻿// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

Shader "HarmonicUnity/HueSprite"
{
    Properties
    {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
        _Color ("Tint", Color) = (1,1,1,1)
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 1
        [HideInInspector] _RendererColor ("RendererColor", Color) = (1,1,1,1)
        [HideInInspector] _Flip ("Flip", Vector) = (1,1,1,1)
        _HueAdjust ("Hue Adjust", Float) = 0
    }

    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

        Cull Off
        Lighting Off
        ZWrite Off
        Fog { Mode Off }
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
        CGPROGRAM
            #pragma vertex FixedSpriteVert
            #pragma fragment frag
            #pragma target 2.0
            #pragma multi_compile_instancing
            #pragma multi_compile_local _ PIXELSNAP_ON
            #include "UnitySprites.cginc"

            // Fixed PixelSnap, handles odd screen resolutions.
            inline float4 FixedPixelSnap (float4 pos)
            {
                float2 hpc = _ScreenParams.xy * 0.5;
                if (_ScreenParams.x % 2 > 0.5) {
                    hpc.x = _ScreenParams.x;
                }
                if (_ScreenParams.y % 2 > 0.5) {
                    hpc.y = _ScreenParams.y;
                }

                float2 pixelPos = (pos.xy / pos.w) * hpc;
                pixelPos = round(pixelPos);
                pos.xy = pixelPos / hpc * pos.w;
                return pos;
            }

            v2f FixedSpriteVert(appdata_t IN)
            {
                v2f OUT;

                UNITY_SETUP_INSTANCE_ID (IN);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);

                OUT.vertex = UnityFlipSprite(IN.vertex, _Flip);
                OUT.vertex = UnityObjectToClipPos(OUT.vertex);
                OUT.texcoord = IN.texcoord;
                OUT.color = IN.color * _Color * _RendererColor;

                #ifdef PIXELSNAP_ON
                OUT.vertex = FixedPixelSnap (OUT.vertex);
                #endif

                return OUT;
            }

            fixed _HueAdjust;

            fixed3 hueShift(in fixed3 col, float hue) {
                const fixed3 k = fixed3(0.57735, 0.57735, 0.57735);
                fixed cosAngle = cos(hue);
                return fixed3(col * cosAngle + cross(k, col) * sin(hue) + k * dot(k, col) * (1.0 - cosAngle));
            }

            fixed4 frag(v2f IN) : SV_Target
            {
                fixed4 texcol = SampleSpriteTexture (IN.texcoord);
                texcol.rgb = hueShift(texcol.rgb, _HueAdjust * 2 * 3.14);
                return texcol * IN.color;
            }

        ENDCG
        }
    }

    Fallback "Sprites/Default"
}
