Shader "HarmonicUnity/Transition-Fade"
{
    Properties
    {
        _Progress ("Progress", Range(0.0, 1.0)) = 0.5
        [MaterialToggle] _Reverse ("Reverse", Float) = 0.0
        [PerRendererData] _MainTex ("Main Texture", 2D) = "white" {}
    }

    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

        Cull Off
        Lighting Off
        ZWrite Off
        Fog { Mode Off }
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
        CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            float _Progress;
            float _Reverse;

            // vertex shader inputs
            struct appdata
            {
                float4 vertex : POSITION; // vertex position
                float2 uv : TEXCOORD0; // texture coordinate
                float4 color : COLOR;
            };
            
            // vertex shader outputs ("vertex to fragment")
            struct v2f
            {
                float2 uv : TEXCOORD0; // texture coordinate
                float4 vertex : SV_POSITION; // clip space position
                float4 color : COLOR;
            };

            // vertex shader
            v2f vert (appdata v)
            {
                v2f o;
                // transform position to clip space
                // (multiply with model*view*projection matrix)
                o.vertex = UnityObjectToClipPos(v.vertex);
                // just pass the texture coordinate
                o.uv = v.uv;
                o.color = v.color;
                return o;
            }            

            fixed4 frag(v2f IN) : SV_Target
            {
                if (_Progress <= 0.0) {
                  discard;
                }
                return float4(IN.color.xyz, IN.color.w * _Progress);
            }

        ENDCG
        }
    }

    Fallback "Sprites/Default"
}
