﻿/*
Defines a jslib plugin that registers the OpenTabOnMouseUp and OpenTabOnKeyUp
functions, which open a new tab on the next mouse up or key up event.
This is used to open links in new tabs properly on WebGL.
See BrowserManager.
*/

var WebGlNewTabPlugin = {  
    OpenTabOnMouseUp:function(url) {
        url = UTF8ToString(url);
        document.onmouseup = function() {
            window.open(url);
            document.onmouseup = null;
        }
    },
    OpenTabOnKeyUp:function(url) {
        url = UTF8ToString(url);
        document.onkeyup = function() {
            window.open(url);
            document.onkeyup = null;
        }
    },
    OpenTabOnMouseOrKeyUp:function(url) {
        url = UTF8ToString(url);
        document.onmouseup = function() {
            window.open(url);
            document.onmouseup = null;
            document.onkeyup = null;
        }
        document.onkeyup = function() {
            window.open(url);
            document.onmouseup = null;
            document.onkeyup = null;
        }
    }
};

mergeInto(LibraryManager.library, WebGlNewTabPlugin);
