﻿/*
Defines a jslib plugin that registers the GetReferrer function, which is used
to determine the referrer url of the embededd game.
See BrowserManager.
*/

var ReferrerUrlPlugin = {  
    GetReferrer:function() {
        var result = document.referrer;
        var bufferSize = lengthBytesUTF8(result) + 1;
        var buffer = _malloc(bufferSize);
        stringToUTF8(result, buffer, bufferSize);
        return buffer;
    }
};

mergeInto(LibraryManager.library, ReferrerUrlPlugin);
