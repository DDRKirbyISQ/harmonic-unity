﻿using UnityEngine;
using System.Runtime.InteropServices;

/// <summary>
/// Utility functions to interact with web browser via javascript plugin code.
/// 
/// OpenNewTabFromMouse and OpenNewTabFromKeyboard are used to open urls in new tabs correctly in a cross-platform way.
/// On WebGL this uses a jslib plugin to open the new tab when you release the mouse button or key.  This is required
/// because the new tab will be blocked by browsers as an unwanted popup unless it is initiated by a mouse or key up
/// event.  There is currently no way to open a new tab from something other than a mouse or key event without running
/// into the popup blocker issue.
/// 
/// GetReferrerUrl simply attempts to return document.referrer, which is used for site locking (See SiteLock).
/// </summary>
public static class BrowserUtils
{
    /// <summary>
    /// Opens the given URL in a new tab, from a mouse down event.
    /// Must be called BEFORE the mouse button is released.
    /// </summary>
    /// <param name="url">URL to open.</param>
    public static void OpenNewTabFromMouse(string url) {
#if UNITY_WEBGL && !UNITY_EDITOR
        OpenTabOnMouseUp(url);
#else
        Application.OpenURL(url);
#endif
    }

    /// <summary>
    /// Opens the given URL in a new tab, from a key down event.
    /// Must be called BEFORE the key is released.
    /// </summary>
    /// <param name="url">URL to open.</param>
    public static void OpenNewTabFromKeyboard(string url) {
#if UNITY_WEBGL && !UNITY_EDITOR
        OpenTabOnKeyUp(url);
#else
        Application.OpenURL(url);
#endif
    }

    /// <summary>
    /// Opens the given URL in a new tab, from a key down event.
    /// Must be called BEFORE the key is released.
    /// </summary>
    /// <param name="url">URL to open.</param>
    public static void OpenNewTabFromKeyboardOrMouse(string url) {
#if UNITY_WEBGL && !UNITY_EDITOR
        OpenTabOnMouseOrKeyUp(url);
#else
        Application.OpenURL(url);
#endif
    }

    /// <summary>
    /// Returns the referrer URL of the page's iframe.
    /// </summary>
    /// <returns>Referrer URL</returns>
    public static string GetReferrerUrl() {
#if UNITY_WEBGL && !UNITY_EDITOR
        return GetReferrer();
#else
        return string.Empty;
#endif
    }

#if UNITY_WEBGL
    [DllImport("__Internal")]
    private static extern void OpenTabOnMouseUp(string url);

    [DllImport("__Internal")]
    private static extern void OpenTabOnKeyUp(string url);

    [DllImport("__Internal")]
    private static extern void OpenTabOnMouseOrKeyUp(string url);

    [DllImport("__Internal")]
    private static extern string GetReferrer();
#endif
}
