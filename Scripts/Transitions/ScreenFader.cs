﻿using UnityEngine;
using UnityEngine.UI;

namespace HarmonicUnity
{
    /// <summary>
    /// A singleton class that fades the screen in or out using a UI image.
    /// 
    /// Usage:
    ///     // Start fading the screen out to black.
    ///     ScreenFader.FadeOut();
    /// 
    ///     // Start fading the screen in again.
    ///     ScreenFader.FadeIn();
    /// 
    ///     // Fade the screen out to red, over 2 seconds.
    ///     ScreenFader.FadeOut(new Color(1.0f, 0.0f, 0.0f), 2.0f);
    /// 
    ///     // Fade in instantly.
    ///     ScreenFader.FadeIn(0.0f);
    /// </summary>
    public class ScreenFader : MonoBehaviour
    {
        public enum TransitionType
        {
            Fade,
            Diamonds,
        }

        /// <summary>
        /// Default duration of fade to use.
        /// </summary>
        const float kDefaultDuration = 1.0f;

        /// <summary>
        /// Default color to fade out to.
        /// </summary>
        static readonly Color kDefaultColor = new Color(0.0f, 0.0f, 0.0f);

        public static bool IsFadingOut { get { return _instance._faderShowing; } }

        static ScreenFader _instance { get { return SingletonBehavior<ScreenFader>.Get("HarmonicUnity/ScreenFader"); } }

        [SerializeField]
        Material _fadeMaterial;

        [SerializeField]
        Material _diamondsMaterial;

        /// <summary>
        /// UI image used to fade.
        /// </summary>
        [SerializeField]
        Image _image;

        /// <summary>
        /// True if fading out, false if fading in.
        /// </summary>
        bool _faderShowing = false;

        /// <summary>
        /// Duration of the current fade, in seconds.
        /// </summary>
        float _fadeDuration;

        float _progress = 0.0f;

        /// <summary>
        /// Changes thea ctive screen transition type.
        /// </summary>
        /// <param name="type">New type to use.</param>
        public static void SetType(TransitionType type) {
            switch (type) {
                case TransitionType.Fade:
                    _instance._image.material = _instance._fadeMaterial;
                    break;
                case TransitionType.Diamonds:
                    _instance._image.material = _instance._diamondsMaterial;
                    break;
                default:
                    Debug.LogErrorFormat("Unhandled TransitionType {0}", type);
                    break;
            }
        }

        /// <summary>
        /// Fades the screen out to black.
        /// </summary>
        /// <param name="duration">Duration of the fade, in seconds.  Defaults to ScreenFader.kDefaultDuration.</param>
        public static void FadeOut(float duration = kDefaultDuration) {
            FadeOut(kDefaultColor, duration);
        }

        /// <summary>
        /// Fades the screen out to a custom color.
        /// </summary>
        /// <param name="color">Color to fade to.</param>
        /// <param name="duration">Duration of the fade, in seconds.  Defaults to ScreenFader.kDefaultDuration.</param>
        public static void FadeOut(Color color, float duration = kDefaultDuration) {
            _instance._faderShowing = true;
            _instance._fadeDuration = duration;
            _instance._image.color = color;
            _instance._progress = duration <= 0.0f ? 1.0f : 0.0f;
            _instance._image.material.SetFloat("_Progress", _instance._progress);
            _instance._image.material.SetFloat("_Reverse", 0.0f);
            _instance.gameObject.SetActive(true);
        }

        /// <summary>
        /// Fades the screen in, after being faded out.
        /// </summary>
        /// <param name="duration">Duration of the fade, in seconds.  Defaults to ScreenFader.kDefaultDuration.</param>
        public static void FadeIn(float duration = kDefaultDuration) {
            _instance._faderShowing = false;
            _instance._fadeDuration = duration;
            if (duration <= 0.0f) {
                _instance._progress = 0.0f;
            }
        }

        protected void ConstructScreenFader() {
            _fadeMaterial = Instantiate(_fadeMaterial);
            _diamondsMaterial = Instantiate(_diamondsMaterial);
            _image.material = _diamondsMaterial;
        }

        protected virtual void Awake() {
            ConstructScreenFader();
        }

        protected virtual void Update() {
            // Disable ourself if the fader isn't needed.
            if (_instance._faderShowing == false && _progress <= 0.0f) {
                gameObject.SetActive(false);
                return;
            }

            if (_fadeDuration <= 0.0f) {
                return;
            }
            float rate = Time.unscaledDeltaTime / _fadeDuration;
            _instance._progress += _faderShowing ? rate : -rate;
            _instance._progress = Mathf.Clamp01(_instance._progress);
            _instance._image.material.SetFloat("_Progress", _instance._progress);
            _instance._image.material.SetFloat("_Reverse", _instance._faderShowing ? 0.0f : 1.0f);
        }
    }
}
