﻿using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;
using System;
using System.Collections;
#if UNITY_SWITCH
using UnityEngine.Switch;
#endif

namespace HarmonicUnity
{
    /// <summary>
    /// A singleton class used for transitioning between different Unity scenes.
    /// Leverages ScreenFader to fade the screen for the transition.
    /// TODO: Allow for transitions other than fading.
    /// </summary>
    public class SceneTransitioner : MonoBehaviour
    {
        /// <summary>
        /// Default transition duration.
        /// </summary>
        const float kDefaultDuration = 1.0f;

        /// <summary>
        /// Default color to fade out to.
        /// </summary>
        static readonly Color kDefaultColor = new Color(0.0f, 0.0f, 0.0f);

        public static bool IsTransitioning { get { return _instance._isTransitioning; } }

        public static bool IsTransitioningOut { get { return _instance._isTransitioningOut; } }

        public static string PreviousSceneName { get { return _instance._previousSceneName; } }

        public static bool IsFullyOut { get { return _instance._isFullyOut; } }

        event Action _onTransition;

        public static event Action OnTransition {
            add { _instance._onTransition += value; }
            remove { _instance._onTransition -= value; }
        }

        static SceneTransitioner _instance {
            get { return SingletonBehavior<SceneTransitioner>.Get("HarmonicUnity/SceneTransitioner"); }
        }

        bool _isTransitioning = false;

        bool _isTransitioningOut = false;

        bool _isFullyOut = false;

        string _previousSceneName = "";

        int _waitCount = 0;

        public static void Init() {
            SingletonBehavior<SceneTransitioner>.Get("HarmonicUnity/SceneTransitioner");
        }

        public static void PushSceneWait() {
            ++_instance._waitCount;
        }

        public static void PopSceneWait() {
            --_instance._waitCount;
        }

        /// <summary>
        /// Transitions to a new scene, fading the screen.
        /// </summary>
        /// <param name="sceneName">Name of scene to transition to.</param>
        /// <param name="duration">Duration of fade.  Defaults to SceneTransitioner.kDefaultDuration.</param>
        /// <param name="fadeMusic">Whether to fade out music as well.  Defaults to true.</param>
        /// <param name="delay">Delay after fade before actually transitioning</param>
        /// <param name="skipOut">If true, instantly fades out.</param>
        public static void Transition(string sceneName, float duration = kDefaultDuration, bool fadeMusic = true, float delay = 0.0f, bool skipOut = false) {
            Transition(sceneName, kDefaultColor, duration, fadeMusic, delay);
        }

        /// <summary>
        /// Transitions to a new scene, fading the screen.
        /// </summary>
        /// <param name="sceneName">Name of scene to transition to.</param>
        /// <param name="color">Color to fade to.  Defaults to SceneTransitioner.kDefaultColor.</param>
        /// <param name="duration">Duration of fade.  Defaults to SceneTransitioner.kDefaultDuration.</param>
        /// <param name="fadeMusic">Whether to fade out music as well.  Defaults to true.</param>
        /// <param name="delay">Delay after fade before actually transitioning</param>
        /// <param name="skipOut">If true, instantly fades out.</param>
        public static void Transition(
            string sceneName,
            Color color,
            float duration = kDefaultDuration,
            bool fadeMusic = true,
            float delay = 0.0f,
            bool skipOut = false
        ) {
            _instance._isTransitioning = true;
            _instance._isTransitioningOut = true;
            _instance._onTransition?.Invoke();

            // Abort all previous scene transitions.
            // TODO: Log a warning and let user control whether new or old transition takes precedence.
            _instance.StopAllCoroutines();

            _instance._previousSceneName = SceneManager.GetActiveScene().name;
            _instance.StartCoroutine(TransitionRoutine(sceneName, color, duration, fadeMusic, delay, skipOut));
        }

        static IEnumerator TransitionRoutine(string sceneName, Color color, float duration, bool fadeMusic, float delay, bool skipOut) {
            if (skipOut) {
                ScreenFader.FadeOut(color, 0.0f);
                AudioManager.StopMusic();
            } else {
                ScreenFader.FadeOut(color, duration);
                if (fadeMusic && AudioManager.IsMusicPlaying()) {
                    AudioManager.FadeMusic(1.0f / duration);
                }
                yield return new WaitForSecondsRealtime(duration);
            }
            _instance._isTransitioningOut = false;
            _instance._isFullyOut = true;

#if UNITY_SWITCH && !UNITY_EDITOR
            Performance.SetCpuBoostMode(Performance.CpuBoostMode.FastLoad);
#endif

            yield return null;
            yield return new WaitForSecondsRealtime(delay);

            yield return SceneManager.LoadSceneAsync("EmptyScene", LoadSceneMode.Single);
            yield return null;

            AsyncOperationHandle<SceneInstance> handle = Addressables.LoadSceneAsync(sceneName);
            yield return handle;
            yield return null;
            handle.Result.ActivateAsync();

            // Wait for scene to fully load.
            for (int i = 0; i < 5; ++i) {
                yield return null;
            }

            yield return new WaitUntil(CanProceed);

#if UNITY_SWITCH && !UNITY_EDITOR
            Performance.SetCpuBoostMode(Performance.CpuBoostMode.Normal);
#endif

            _instance._isFullyOut = false;
            ScreenFader.FadeIn(duration);
            yield return new WaitForSecondsRealtime(duration);
            _instance._isTransitioning = false;
            yield break;
        }

        protected void ConstructSceneTransitioner() {
        }

        protected virtual void Awake() {
            ConstructSceneTransitioner();
        }

        public static bool CanProceed() {
            return _instance._waitCount <= 0;
        }
    }
}
