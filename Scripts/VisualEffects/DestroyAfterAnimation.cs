﻿using UnityEngine;

namespace HarmonicUnity
{
    /// <summary>
    /// A simple script that automatically deletes the attached GameObject after its AnimatorManager is
    /// done playing its animation.
    /// </summary>
    [RequireComponent(typeof(AnimatorManager))]
    public class DestroyAfterAnimation : MonoBehaviour
    {
        AnimatorManager _animatorManager;

        protected void ConstructDestroyAfterAnimation()
        {
            _animatorManager = this.GetRequiredComponent<AnimatorManager>();
        }

        protected virtual void Awake() {
            ConstructDestroyAfterAnimation();
        }

        protected virtual void Update() {
            if (_animatorManager.IsDone()) {
                Destroy(gameObject);
            }
        }
    }
}
