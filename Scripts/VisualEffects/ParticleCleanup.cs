﻿using UnityEngine;

namespace HarmonicUnity
{
    /// <summary>
    /// A simple script that automatically deletes its GameObject when an attached particle system has finished.
    /// </summary>
    public class ParticleCleanup : MonoBehaviour
    {
        protected virtual void Start() {
            ParticleSystem system = this.GetRequiredComponent<ParticleSystem>();

            // TODO: Doesn't work with lifetime curves.
            float time = system.main.duration
                         + system.main.startLifetime.constant
                         + system.main.startLifetime.constantMax;
            Destroy(gameObject, time);
        }
    }
}
