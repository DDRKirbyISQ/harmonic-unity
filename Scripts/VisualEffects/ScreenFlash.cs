﻿using UnityEngine;
using UnityEngine.UI;

namespace HarmonicUnity
{
    /// <summary>
    /// Creates a fading screen flash.
    ///
    /// Usage:
    ///
    ///     // Create a white screen flash.
    ///     ScreenFlash.Create();
    ///
    ///     // Create a 50% opacity blue screen flash that fades in 2 seconds.
    ///     ScreenFlash.Create(new Color(0.0f, 0.0f, 1.0f, 0.5f), 2.0f);
    /// </summary>
    public class ScreenFlash : MonoBehaviour
    {
        /// <summary>
        /// Default duration of flash.
        /// </summary>
        const float kDefaultDuration = 1.0f;

        /// <summary>
        /// Default color of flash.
        /// </summary>
        static readonly Color kDefaultColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);

        static ScreenFlash _instance {
            get { return SingletonBehavior<ScreenFlash>.Get("HarmonicUnity/ScreenFlash"); }
        }

        /// <summary>
        /// UI image used for flash.
        /// </summary>
        [SerializeField]
        Image _image;

        /// <summary>
        /// Duration of flash.
        /// </summary>
        float _duration;

        public static void Init() {
            SingletonBehavior<ScreenFlash>.Get("HarmonicUnity/ScreenFlash");
        }

        /// <summary>
        /// Creates a white screen flash.
        /// </summary>
        /// <param name="duration">Duration of flash, in seconds.</param>
        /// <returns>The new ScreenFlash instance.</returns>
        public static void Flash(float duration = kDefaultDuration) {
            ScreenFlash.Flash(kDefaultColor, duration);
        }

        /// <summary>
        /// Creates a screen flash.
        /// </summary>
        /// <param name="color">Color of flash.</param>
        /// <param name="duration">Duration of flash, in seconds.</param>
        /// <returns>The new ScreenFlash instance.</returns>
        public static void Flash(Color color, float duration = kDefaultDuration) {
            _instance.DoFlash(color, duration);
        }


        /// <summary>
        /// Executes a screen flash.
        /// </summary>
        /// <param name="color">Color of flash.</param>
        /// <param name="duration">Duration of flash, in seconds.</param>
        void DoFlash(Color color, float duration) {
            // Sanity check duration.
            if (duration <= 0.0) {
                Debug.LogErrorFormat(
                    "Invalid duration {0} for ScreenFlash, using {1} instead",
                    duration,
                    kDefaultDuration
                );
                duration = kDefaultDuration;
            }

            _image.color = color;
            _duration = duration;
            enabled = true;
        }

        protected virtual void Update() {
            float alpha = _image.color.a;
            float rate = Time.unscaledDeltaTime / _duration;
            alpha -= rate;
            alpha = Mathf.Clamp01(alpha);
            _image.color = new Color(_image.color.r, _image.color.g, _image.color.b, alpha);
            enabled = alpha > 0.0f;
        }
    }
}
