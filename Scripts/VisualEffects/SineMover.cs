﻿using UnityEngine;

/// <summary>
/// Moves the local transform of a GameObject via a sine function.
/// </summary>
public class SineMover : MonoBehaviour
{
    [SerializeField]
    Vector3 _amount;

    [SerializeField]
    float _period = 1.0f;

    [SerializeField]
    bool _bidirectional = true;

    Vector3 _initialPosition;

    protected void ConstructSineMover() {
        _initialPosition = transform.localPosition;
    }

    protected virtual void Awake() {
        ConstructSineMover();
    }

    protected virtual void Update() {
        float progress = Mathf.Sin(Time.timeSinceLevelLoad * 2 * Mathf.PI / _period);
        if (!_bidirectional) {
            progress = progress * 0.5f + 0.5f;
        }
            
        transform.localPosition = progress * _amount + _initialPosition;
    }
}
