﻿using UnityEngine;

/// <summary>
/// Pulses the alpha of a GameObject and its children using LeanTween.
/// </summary>
public class TweenFader : MonoBehaviour
{
    [SerializeField]
    float _minAlpha = 0.0f;

    [SerializeField]
    float _maxAlpha = 1.0f;

    [SerializeField]
    float _fadeDuration = 1.0f;

    [SerializeField]
    LeanTweenType _tweenType = LeanTweenType.easeInOutSine;

    protected virtual void Start() {
        RectTransform rectTransform = GetComponent<RectTransform>();
        if (rectTransform != null) {
            LeanTween.alpha(rectTransform, _maxAlpha, 0.0f).setOnComplete(() => {
                LeanTween.alpha(rectTransform, _minAlpha, _fadeDuration).setEase(_tweenType).setLoopPingPong();
            });
        } else {
            LeanTween.alpha(gameObject, _maxAlpha, 0.0f).setOnComplete(() => {
                LeanTween.alpha(gameObject, _minAlpha, _fadeDuration).setEase(_tweenType).setLoopPingPong();
            });
        }
    }
}
