﻿using UnityEngine;

namespace HarmonicUnity
{
    /// <summary>
    /// A simple script that automatically deletes the attached GameObject after a given amount of time.
    /// </summary>
    public class DestroyAfterSeconds : MonoBehaviour
    {
        [SerializeField]
        float _time = 1.0f;

        protected virtual void Start() {
            Destroy(gameObject, _time);
        }
    }
}
