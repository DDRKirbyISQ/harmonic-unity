﻿using UnityEngine;

public class TransformShaker : MonoBehaviour
{
    float _amount;

    float _endTime;

    public static TransformShaker Shake(float amount, float duration, Transform transform) {
        TransformShaker result = transform.gameObject.AddComponent<TransformShaker>();
        result.ConstructTransformShaker(amount, duration);
        return result;
    }

    protected void ConstructTransformShaker(float amount, float duration) {
        _amount = amount;
        _endTime = Time.time + duration;
    }

    protected virtual void Update() {
        if (Time.time > _endTime) {
            transform.localPosition = Vector3.zero;
            Destroy(this);
            return;
        }

        if (Time.timeScale == 0.0f) {
            // Paused, don't update.
            return;
        }

        transform.localPosition = Random.insideUnitCircle * _amount;
    }
}
