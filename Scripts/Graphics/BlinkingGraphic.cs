﻿using UnityEngine;
using UnityEngine.UI;
using HarmonicUnity;

public class BlinkingGraphic : MonoBehaviour
{
    [SerializeField]
    float _blinkInterval = 1.0f;

    [SerializeField]
    bool _useUnscaledTime = true;

    Graphic _graphic;

    protected void ConstructBlinkingText() {
        _graphic = this.GetRequiredComponent<Graphic>();
    }

    protected virtual void Awake() {
        ConstructBlinkingText();
    }

    protected virtual void Update() {
        float time = _useUnscaledTime ? Time.unscaledTime : Time.time;
        _graphic.enabled = time % _blinkInterval < _blinkInterval / 2;
    }
}
