﻿using UnityEngine;

namespace HarmonicUnity
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class Backdrop : MonoBehaviour
    {
        public Vector2 Offset;

        public Vector2 NonScaledOffset;

        public bool IsTutorialFloor = false;

        [SerializeField]
        bool _wrapX = true;

        [SerializeField]
        bool _wrapY = false;

        [SerializeField]
        Vector2 _cameraScrollRate = Vector2.one;

        Vector2 _spriteSize;

        SpriteRenderer _renderer;

        public void ForceUpdate() {
            LateUpdate();
        }

        protected void ConstructBackdrop() {
            _renderer = this.GetRequiredComponent<SpriteRenderer>();
        }

        protected virtual void Awake() {
            ConstructBackdrop();
        }

        protected virtual void Start() {
            _spriteSize = _renderer.sprite.bounds.size;

            if (_renderer.sortingOrder < -1) {
                gameObject.layer = LayerMask.NameToLayer("MenuBackdropsBack");
            }
        }

        protected virtual void LateUpdate() {
            // Make sure sprite tiling width is at least twice the camera size.
            PixelPerfectCamera pixelPerfectCamera = Camera.main.GetComponent<PixelPerfectCamera>();
            float cameraWidth = pixelPerfectCamera.cameraSize.x * 2;
            float width = Mathf.CeilToInt(cameraWidth * 2 / _spriteSize.x) * _spriteSize.x;
            if (!Mathf.Approximately(_renderer.size.x, width)) {
                _renderer.size = new Vector2(width, _renderer.size.y);
            }

            Vector3 cameraPosition = Camera.main.transform.position;
            float x = (-cameraPosition.x + Offset.x) * _cameraScrollRate.x + NonScaledOffset.x;
            if (_wrapX) {
                x = Wrap(x, _spriteSize.x);
            }
            float y = (-cameraPosition.y + Offset.y) * _cameraScrollRate.y + NonScaledOffset.y;
            if (_wrapY) {
                y = Wrap(y, _spriteSize.y);
            }
            transform.position = cameraPosition + new Vector3(x, y, 0.0f);
        }

        float Wrap(float value, float size) {
            // Normalize, add 1.0f.
            value /= size;
            value += 0.5f;

            // Wrap to 0-1.
            value = value - Mathf.Floor(value);

            // Turn 0 into 1.
            //value = value - Mathf.Ceil(value) + 1;

            // Subtract 1.0f, denormalize.
            value -= 0.5f;
            value *= size;

            return value;
        }
    }
}
