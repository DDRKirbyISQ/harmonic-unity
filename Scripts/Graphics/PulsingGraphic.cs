﻿using UnityEngine;
using UnityEngine.UI;
using HarmonicUnity;

public class PulsingGraphic : MonoBehaviour
{
    [SerializeField]
    Color _color1 = Color.white;

    [SerializeField]
    Color _color2 = new Color(1.0f, 1.0f, 1.0f, 0.5f);

    [SerializeField]
    float _period = 1.0f;

    [SerializeField]
    bool _useUnscaledTime = true;

    Graphic _graphic;

    float _startTime;

    protected virtual void Awake() {
        _graphic = this.GetRequiredComponent<Graphic>();
        _startTime = _useUnscaledTime ? Time.unscaledTime : Time.time;
    }

    protected virtual void Update() {
        float time = _useUnscaledTime ? Time.unscaledTime : Time.time;
        time -= _startTime;
        float progress = -Mathf.Cos(time * 2 * Mathf.PI / _period) * 0.5f + 0.5f;
        _graphic.color = Color.Lerp(_color1, _color2, progress);
    }
}
