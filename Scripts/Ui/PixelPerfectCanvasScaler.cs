﻿using UnityEngine;
using UnityEngine.UI;

namespace HarmonicUnity
{
    /// <summary>
    /// Scales a CanvasScaler based on the main camera's orthographic size.
    /// </summary>
    [ExecuteInEditMode]
    [RequireComponent(typeof(CanvasScaler))]
    public class PixelPerfectCanvasScaler : MonoBehaviour
    {
        public float adjustment = 0.0f;

        CanvasScaler _scaler;

        PixelPerfectCamera _pixelPerfectCamera;

        protected virtual void Awake() {
            _scaler = this.GetRequiredComponent<CanvasScaler>();
            _pixelPerfectCamera = Camera.main.GetComponent<PixelPerfectCamera>();
            if (_pixelPerfectCamera != null) {
                _pixelPerfectCamera.adjustCameraFOV();
            }
            Resize();
        }

        protected virtual void LateUpdate() {
            Resize();
        }

        public void Resize() {
            if (Camera.main == null) {
                return;
            }
            float size = Camera.main.orthographicSize;
            if (_pixelPerfectCamera != null) {
                size = _pixelPerfectCamera.uiSize;
            }
            float scale = Camera.main.pixelHeight * 0.5f / size;
            scale += adjustment;
            scale = Mathf.Round(scale * 10) * 0.1f;
            if (_scaler.scaleFactor != scale) {
                _scaler.scaleFactor = scale;
            }
        }
    }
}
