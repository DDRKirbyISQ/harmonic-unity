﻿using UnityEngine;
using UnityEngine.UI;

namespace HarmonicUnity
{
    /// <summary>
    /// Automatically scales the attached CanvasScaler based on an AutomaticLetterboxer attached to the canvas's camera.
    /// </summary>
    [ExecuteInEditMode]
    [RequireComponent(typeof(CanvasScaler))]
    [RequireComponent(typeof(Canvas))]
    public class LetterboxedCanvasScaler : MonoBehaviour
    {
        CanvasScaler _scaler;

        Canvas _canvas;

        protected virtual void Awake() {
            _scaler = this.GetRequiredComponent<CanvasScaler>();
            _canvas = this.GetRequiredComponent<Canvas>();
        }

        protected virtual void LateUpdate() {
            if (_canvas.renderMode == RenderMode.WorldSpace) {
                return;
            }

            Camera camera;
            if (_canvas.renderMode == RenderMode.ScreenSpaceCamera) {
                camera = _canvas.worldCamera;
            } else {
                camera = Camera.main;
            }

            _scaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
            AutomaticLetterboxer letterboxer = camera.gameObject.GetRequiredComponent<AutomaticLetterboxer>();
            _scaler.referenceResolution = letterboxer.BaseResolution;
            _scaler.matchWidthOrHeight = letterboxer.UsingHeight() ? 0.0f : 1.0f;
        }
    }
}
