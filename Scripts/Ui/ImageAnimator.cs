using UnityEngine;
using UnityEngine.UI;

namespace HarmonicUnity
{
    [RequireComponent(typeof(Image))]
    public class ImageAnimator : MonoBehaviour
    {
        [SerializeField]
        float _animFps = 30.0f;

        [SerializeField]
        Sprite[] _sprites;

        Image _image;

        int _index = 0;

        float _time = 0.0f;

        protected virtual void Start() {
            _image = this.GetRequiredComponent<Image>();
        }

        protected virtual void Update() {
            _time += Time.unscaledDeltaTime;
            while (_time > 1.0f / _animFps) {
                _time -= 1.0f / _animFps;
                _index = (_index + 1) % _sprites.Length;
            }
            _image.sprite = _sprites[_index];
        }
    }
}
