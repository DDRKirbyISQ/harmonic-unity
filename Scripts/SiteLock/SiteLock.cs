﻿using UnityEngine;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace HarmonicUnity
{
    /// <summary>
    /// Provides site locking for webgl games by checking Application.absoluteURL and document.referrer
    /// (via javascript).  You can use this to halt the game and/or show a copy protection violation message
    /// when your game is hosted on a site that you don't explicitly allow.
    /// 
    /// ===============================================================================================================
    /// 
    /// Site locking is done by providing two whitelists in the form of regexes.  Though regex syntax can be a bit
    /// complex (especially when they must be escaped in C# string form), this is in general the most flexible way to
    /// construct a whitelist that may have elements such as *.somedomain.com/* as well as mydomain.com/*.  Common
    /// examples are provided below.
    /// 
    /// Two separate tests are done -- one against Application.absoluteURL and one against document.referrer.
    /// These two checks are different and should be provided different whitelists.
    /// 
    /// WebGL games are typically loaded by using an iframe in the containing webpage.
    /// For example, let's say we have a game build located at "https://somedomain.com/mygame/webgl/index.html".
    /// We want to embed this game inside a nice webpage (with some links, a description, etc) at "https://somedomain.com/mygame/mygame.html".
    /// 
    /// Thus, "https://somedomain.com/mygame/mygame.html" would contain an iframe somewhere in the source that looks like this:
    /// 
    ///     <iframe src="https://somedomain.com/mygame/webgl/index.html"></iframe>
    /// 
    /// Application.absoluteURL is the actual page that contains the WebGL player -- in other words, it is the direct
    /// URL of your "index.html" page.  In this case, it would be "https://somedomain.com/mygame/webgl/index.html".
    /// 
    /// document.referrer is blank when loading the index.html page directly, but will be set to the parent page when
    /// loading it via an iframe.  In this case, it would be "https://somedomain.com/mygame/mygame.html".
    /// 
    /// This is important because if we were to only check Application.absoluteURL, then a malicious site such as
    /// "http://evildomain.com" could simply copy-paste your iframe code and Application.absoluteURL would still return
    /// "https://somedomain.com/mygame/webgl/index.html" just as before.  So in this case we need to check
    /// document.referrer to prevent the iframe embed from "http://evildomain.com".
    /// 
    /// We thus have the following basic rules for site-locking:
    /// - Always serve your game using an iframe, where the parent page is on a trusted domain.
    /// - Check document.referrer against your trusted domain(s) for normal usage.
    /// - Use Application.absoluteURL to check for alternative usage, such as loading the game from a file:/// URL or
    ///   from localhost/
    /// 
    /// Note that it is not currently possible to prevent a nested iframe exploit using this class.
    /// For example, http://evildomain.com could embed an iframe that points to
    /// "https://somedomain.com/mygame/mygame.html" and your game would not be able to do anything about it.
    /// Preventing this sort of iframe embed is outside the scope of what Unity can do -- please look into using the
    /// "frame-ancestors" content security policy directive instead.
    /// 
    /// A note: We use document.referrer instead of attempting to directly access the parent page's url because that is
    /// not possible for cross-domain embeds.  For hosting games yourself this is not an issue, but this becomes
    /// prohibitive for sites like itch.io that serve WebGL builds from a different domain than the actual website.
    /// For example, an itch.io game page like "https://username.itch.io/my-game" would embed an iframe that points to
    /// "https://v6p9d9t4.ssl.hwcdn.net/html/1633230-125353/index.html".  So in this case we actually need to allow
    /// cross-domain embeds while still checking the referrer URL to make sure it's under "https://username.itch.io".
    /// 
    /// If the game is not running as a WebGL build, this becomes a no-op.
    /// Note that there is no way to determine whether your game is running inside of an iframe.  In this
    /// case the Application.absoluteURL that is checked is the iframe's src url.
    /// 
    /// ===============================================================================================================
    /// 
    /// This class will automatically trim off "http://", "https://", "http://www.", and "https://www." prefixes from
    /// any and all urls, so please do not include them in your regexes.
    /// 
    /// Usage:
    /// 
    /// // Define the regexes for allowed domains to check.
    /// static readonly string[] kAllowedReferrers = {
    ///     "^mydomain\\.com/.*$",
    ///     "^myotherdomain\\.com/.*$",
    /// };
    /// static readonly string[] kAllowedUrls = {
    ///     "^file:\\/\\/\\/.*$",
    ///     "^localhost[:\\/].*$",
    ///     "^itch-cave:\\/\\/.*$",
    /// };
    /// 
    /// // Check for a violation.
    /// if (SiteLock.HasViolation(kAllowedReferrers, kAllowedUrls)) {
    ///     // Load some error page that stops the game.
    ///     SceneManager.LoadScene("CopyProtectionScene");
    ///     return;
    /// }
    /// 
    /// ===============================================================================================================
    /// 
    /// Typical allowedReferrer regexes:
    /// 
    /// "^mydomain\\.com/.*$"         - Tests for mydomain.com/*
    /// "^[^/]*\\.mydomain\\.com/.*$" - Tests for *.mydomain.com/*
    /// 
    /// Typical allowedUrl regexes:
    /// 
    /// "^localhost[:\\/].*$"   - Tests for localhost:* or localhost/* (you can also test against 127.0.0.1 if you'd like)
    /// "^file:\\/\\/\\/.*$"    - Tests for file:///*
    /// "^itch-cave:\\/\\/.*$"  - Tests for itch-cave://* (used when running itch.io html5 games locally via the itch app)
    ///  
    /// </summary>
    public static class SiteLock
    {
        static readonly string[] kPrefixes = new string[] {
            "http://www.",
            "https://www.",
            "http://",
            "https://",
        };

        /// <summary>
        /// Determines if there is a site lock violation.
        /// At least one of the given allowedReferrers or allowedUrls must match in order to prevent a violation.
        /// Always returns false for non WebGL builds.
        /// </summary>
        /// <returns>True if the game is running on an unallowed domain, false otherwise.</returns>
        /// <param name="allowedReferrers">Regexes of allowed domains, e.g. "^mydomain\\.com/.*$".</param>
        /// <param name="allowedUrls">Regexes of allowed direct url paths, e.g. "^localhost[:\\/].*$".</param>
        public static bool HasViolation(IEnumerable<string> allowedReferrers, IEnumerable<string> allowedUrls) {
            if (PlatformUtils.CurrentTarget() != BuildTarget.WebGl) {
                return false;
            }

            return !ContainsMatch(BrowserUtils.GetReferrerUrl(), allowedReferrers) &&
                !ContainsMatch(Application.absoluteURL, allowedUrls);
        }

        static bool ContainsMatch(string url, IEnumerable<string> whitelist) {
            if (string.IsNullOrEmpty(url)) {
                return false;
            }

            // Strip prefixes.
            foreach (string prefix in kPrefixes) {
                if (url.StartsWith(prefix)) {
                    url = url.Remove(0, prefix.Length);
                    break;
                }
            }

            // Test each regex.
            foreach (string regexString in whitelist) {
                Regex regex = new Regex(regexString);
                if (regex.IsMatch(url)) {
                    return true;
                }
            }

            return false;
        }
    }
}
