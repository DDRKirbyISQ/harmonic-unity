﻿using UnityEngine;

namespace HarmonicUnity
{
    public class AnimatorManager : MonoBehaviour
    {
        public enum Transition
        {
            DontRepeat,
            KeepProgress,
            Reset,
        }

        public string State {
            get { return _state; }
            set { Play(value); }
        }

        [SerializeField]
        string _initialState;

        string _state;

        string _nextState = null;

        Animator _animator;

        AnimatorStateInfo _info { get { return _animator.GetCurrentAnimatorStateInfo(0); } }

        public void Play(string state, Transition transition = Transition.DontRepeat, string nextState = null, float progress = 0.0f) {
            if (transition == Transition.DontRepeat && _state == state) {
                return;
            }

            if (transition == Transition.Reset) {
                _animator.Play(state, 0, progress);
            } else if (transition == Transition.KeepProgress) {
                _animator.Play(state, 0, _info.normalizedTime);
            } else {
                _animator.Play(state, 0);
            }
            _animator.Update(0.0f);
            _state = state;
            _nextState = nextState;
            enabled = _nextState != null;
        }

        public void PlayAndResume(string state, Transition transition = Transition.DontRepeat) {
            Play(state, transition, _state);
        }

        public bool IsDone() {
            return _info.normalizedTime >= 1.0f;
        }

        public bool HasState(string state) {
            return _animator.HasState(0, Animator.StringToHash(state));
        }

        public void TriggerReset() {
            Start();
        }

        protected void ConstructAnimatorManager() {
            _animator = this.GetRequiredComponent<Animator>();
        }

        protected virtual void Awake() {
            ConstructAnimatorManager();
        }

        protected virtual void Start() {
            if (!string.IsNullOrEmpty(_initialState)) {
                Play(_initialState, Transition.Reset);
            }
            enabled = false;
        }

        protected virtual void Update() {
            if (_nextState != null && IsDone()) {
                Play(_nextState);
            }
        }
    }
}
