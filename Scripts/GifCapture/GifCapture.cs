using UnityEngine;
using System.IO;

namespace HarmonicUnity
{
    /// <summary>
    /// Singleton class for continuously capturing screenshots for making an animated GIF.
    /// The screenshots will be output into a directory called GifCapture/.
    /// Note: You'll have to use some other program to actually form the GIF from the image sequence.
    /// 
    /// Usage:
    ///     // Start recording.
    ///     inputActions.Actions.StartRecording.performed += context => { GifCapture.StartRecording(); };
    ///
    ///     // Stop recording.
    ///     inputActions.Actions.StopRecording.performed += context => { GifCapture.StopRecording(); };
    ///
    ///     // Take a single screenshot.
    ///     inputActions.Actions.Screenshot.performed += context => { GifCapture.TakeScreenshot(); };
    ///
    ///     // Record as long as some key is held down.
    ///     if (inputActions.Actions.Screenshots.ReadValue<float>() > 0.5f {
    ///       GifCapture.TakeScreenshot();
    ///     }
    /// </summary>
    public class GifCapture : MonoBehaviour
    {
        static GifCapture _instance {
            get { return SingletonBehavior<GifCapture>.Get(); }
        }

        /// <summary>
        /// Set to true when continuously capturing screenshots.
        /// </summary>
        bool _recording = false;

        int _superSize;

        int _index = 0;

        public static void Init() {
            SingletonBehavior<GifCapture>.Get();
        }

        /// <summary>
        /// Starts recording.
        /// </summary>
        public static void StartRecording(int superSize = 1) {
            _instance._recording = true;
            _instance._superSize = superSize;
        }

        public static void StopRecording() {
            _instance._recording = false;
        }

        public static void TakeScreenshot(int superSize = 1) {
            _instance._superSize = superSize;
            _instance.Screenshot();
        }

        protected virtual void Update() {
            if (_recording) {
                Screenshot();
            }
        }

        void Screenshot() {
            if (!Directory.Exists("GifCapture")) {
                Directory.CreateDirectory("GifCapture");
            }

            string filename;
            while (true) {
                filename = string.Format("GifCapture/screenshot_{0}.png", _instance._index.ToString("0000"));
                if (_index > 9999) {
                    Debug.LogError("Maximum screenshot limit reached for GifCapture.");
                    return;
                }
                if (!File.Exists(filename)) {
                    break;
                }
                ++_index;
            }
            ScreenCapture.CaptureScreenshot(filename, _superSize);
            ++_index;
        }
    }
}
