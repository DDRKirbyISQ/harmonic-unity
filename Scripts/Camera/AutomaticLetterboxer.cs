﻿using UnityEngine;

namespace HarmonicUnity
{
    /// <summary>
    /// Automatically rescales an attached orthographic camera to fit the entire content as specified by the
    /// _baseResolution.
    /// </summary>
    [ExecuteInEditMode]
    [RequireComponent(typeof(Camera))]
    public class AutomaticLetterboxer : MonoBehaviour
    {
        [SerializeField]
        Vector2 _baseResolution = new Vector2(1000, 600);

        Camera _camera;

        public Vector2 BaseResolution { get { return _baseResolution; } }

        public bool UsingHeight() {
            float aspect = _camera.aspect;
            float halfHeightFromY = _baseResolution.y * 0.5f;
            float halfWidthFromX = _baseResolution.x * 0.5f;
            float halfHeightFromX = halfWidthFromX / aspect;
            return halfHeightFromY < halfHeightFromX;
        }

        protected virtual void ConstructAutomaticLetterboxer() {
            _camera = this.GetRequiredComponent<Camera>();
        }

        protected virtual void Awake() {
            ConstructAutomaticLetterboxer();
        }

        protected virtual void Update() {
            float aspect = _camera.aspect;
            float halfHeightFromY = _baseResolution.y * 0.5f;
            float halfWidthFromX = _baseResolution.x * 0.5f;
            float halfHeightFromX = halfWidthFromX / aspect;
            _camera.orthographicSize = Mathf.Max(halfHeightFromX, halfHeightFromY);
        }
    }
}
