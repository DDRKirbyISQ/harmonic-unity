﻿using UnityEngine;
using HarmonicUnity;

public class CameraFollower : MonoBehaviour
{
    [SerializeField]
    bool _followX = true;

    [SerializeField]
    bool _followY = true;

    protected virtual void LateUpdate() {
        if (_followX) {
            transform.SetPositionX(Camera.main.transform.position.x);
        }
        if (_followY) {
            transform.SetPositionY(Camera.main.transform.position.y);
        }
    }
}
