﻿using UnityEngine;

namespace HarmonicUnity
{
    /// <summary>
    /// An enum for representing the four cardinal directions.
    /// </summary>
    public enum Direction4
    {
        Down,
        Left,
        Right,
        Up,
    }

    public static class Direction4Utils
    {
        public static Direction4 FromVector2(Vector2 vector) {
            float angle = vector.Angle();
            if (angle > 315.0f || angle <= 45.0f) {
                return Direction4.Right;
            } else if (angle <= 135.0f) {
                return Direction4.Up;
            } else if (angle <= 225.0f) {
                return Direction4.Left;
            } else {
                return Direction4.Down;
            }
        }
    }

    public static class Direction4Extensions
    {
        public static Vector2 ToVector2(this Direction4 direction, float length = 1.0f) {
            switch (direction) {
            case Direction4.Down:
                return new Vector2(0.0f, -length);
            case Direction4.Left:
                return new Vector2(-length, 0.0f);
            case Direction4.Right:
                return new Vector2(length, 0.0f);
            case Direction4.Up:
                return new Vector2(0.0f, length);
            default:
                Debug.LogErrorFormat("Unhandled enum value: {0}", direction);
                return new Vector2(0.0f, 0.0f);
            }
        }

        public static Direction4 Opposite(this Direction4 direction) {
            switch (direction) {
            case Direction4.Down:
                return Direction4.Up;
            case Direction4.Left:
                return Direction4.Right;
            case Direction4.Right:
                return Direction4.Left;
            case Direction4.Up:
                return Direction4.Down;
            default:
                Debug.LogErrorFormat("Unhandled enum value: {0}", direction);
                return Direction4.Down;
            }
        }

        public static Direction4 Clockwise(this Direction4 direction) {
            switch (direction) {
            case Direction4.Down:
                return Direction4.Left;
            case Direction4.Left:
                return Direction4.Up;
            case Direction4.Right:
                return Direction4.Down;
            case Direction4.Up:
                return Direction4.Right;
            default:
                Debug.LogErrorFormat("Unhandled enum value: {0}", direction);
                return Direction4.Down;
            }
        }

        public static Direction4 CounterClockwise(this Direction4 direction) {
            switch (direction) {
            case Direction4.Down:
                return Direction4.Right;
            case Direction4.Left:
                return Direction4.Down;
            case Direction4.Right:
                return Direction4.Up;
            case Direction4.Up:
                return Direction4.Left;
            default:
                Debug.LogErrorFormat("Unhandled enum value: {0}", direction);
                return Direction4.Down;
            }
        }

        public static bool IsHorizontal(this Direction4 direction) {
            return direction == Direction4.Left || direction == Direction4.Right;
        }

        public static bool IsVertical(this Direction4 direction) {
            return direction == Direction4.Up || direction == Direction4.Down;
        }
    }
}