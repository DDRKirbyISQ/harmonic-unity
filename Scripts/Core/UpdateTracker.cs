﻿using UnityEngine;

namespace HarmonicUnity
{
    /// <summary>
    /// Update tracker.
    /// Note: FixedUpdate will always run *before* Update for a given Time.unscaledTime value.
    /// TODO: Use Time.inFixedTimeStep to make this easier.
    /// </summary>
    public class UpdateTracker : MonoBehaviour
    {
        /// <summary>
        /// Enum to distinguish between Update cycles and FixedUpdate cycles.
        /// </summary>
        public enum UpdateType
        {
            Update,
            FixedUpdate,
            LateUpdate
        }

        static UpdateTracker _instance {
            get { return SingletonBehavior<UpdateTracker>.Get(); }
        }

        /// <summary>
        /// Set to UpdateType.Update during Update() ticks, and UpdateType.FixedUpdate during FixedUpdate() ticks.
        /// Needed because input handling is tricky during FixedUpdate() calls.
        /// </summary>
        UpdateType _updateType = UpdateType.Update;

        float _lastFixedUpdateTime = -1.0f;

        float _currentFixedUpdateTime = -1.0f;

        public static UpdateType CurrentType() {
            // TODO: Verify that we've already updated this frame?
            return _instance._updateType;
        }

        public static bool IsFixedUpdate() {
            // TODO: Verify that we've already updated this frame?

            return _instance._updateType == UpdateType.FixedUpdate;
        }

        public static float LastFixedUpdateTime() {
            return _instance._lastFixedUpdateTime;
        }

        protected virtual void Update() {
            _updateType = UpdateType.Update;
        }

        protected virtual void LateUpdate() {
            _updateType = UpdateType.LateUpdate;
        }

        protected virtual void FixedUpdate() {
            _updateType = UpdateType.FixedUpdate;

            _lastFixedUpdateTime = _currentFixedUpdateTime;
            _currentFixedUpdateTime = Time.unscaledTime;
        }
    }
}
