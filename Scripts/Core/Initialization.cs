﻿namespace HarmonicUnity
{
    public static class Initialization
    {
        static bool _initialized = false;

        /// <summary>
        /// Performs some preinitialization for various HarmonicUnity singletons and subsystems.
        /// This should be done on application startup.
        /// It is perfectly okay to skip this call, especially when running the game from the editor,
        /// but it means that the first calls to some of these subsystems may incur some additional latency,
        /// processing time, or memory allocation.
        /// </summary>
        public static void Init() {
            if (_initialized) {
                return;
            }

            LeanTween.init();
            AudioManager.Init();
            SceneTransitioner.Init();
            AudioDspTimeKeeper.Init();
            ScreenFlash.Init();
            _initialized = true;
        }
    }
}
