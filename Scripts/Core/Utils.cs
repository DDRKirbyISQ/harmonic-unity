﻿using UnityEngine;
using UnityEngine.Assertions;

namespace HarmonicUnity
{
    /// <summary>
    /// Provides various utility methods.
    /// </summary>
    public static class Utils
    {
        /// <summary>
        /// Returns the positive modulus of two numbers.
        /// </summary>
        /// <returns>Number within the range [0.0f, mod).</returns>
        /// <param name="value">Value.</param>
        /// <param name="mod">Modulo.</param>
        public static float ModPositive(float value, float mod) {
#if UNITY_EDITOR
            Assert.AreNotApproximatelyEqual(mod, 0.0f);
#endif
            if (mod < 0) {
                mod = -mod;
            }
            float result = value % mod;
            return result < 0.0f ? result + mod : result;
        }

        /// <summary>
        /// Returns the positive modulus of two numbers.
        /// </summary>
        /// <returns>Number within the range [0, mod).</returns>
        /// <param name="value">Value.</param>
        /// <param name="mod">Modulo.</param>
        public static int ModPositiveInt(int value, int mod) {
#if UNITY_EDITOR
            Assert.AreNotEqual(mod, 0);
#endif
            if (mod < 0) {
                mod = -mod;
            }
            int result = value % mod;
            return result < 0 ? result + mod : result;
        }

        /// <summary>
        /// Instantiates an instance of a prefab.
        /// </summary>
        /// <param name="prefabPath">Path to prefab, under Resources/.</param>
        /// <returns>The instantiated GameObject.</returns>
        public static GameObject InstantiatePrefab(string prefabPath) {
            GameObject prefab = Resources.Load(prefabPath) as GameObject;
#if UNITY_EDITOR
            Assert.IsNotNull(prefab, string.Format("Failed to load prefab at path: {0}", prefabPath));
#endif
            return Object.Instantiate(prefab);
        }

        /// <summary>
        /// Instantiates an instance of a prefab, returning an attached script of the specified type.
        /// </summary>
        /// <typeparam name="T">Type of script to retrieve.</typeparam>
        /// <param name="prefabPath">Path to prefab, under Resources/.</param>
        /// <returns>The attached script.</returns>
        public static T InstantiatePrefab<T>(string prefabPath) where T : Component {
            GameObject instance = InstantiatePrefab(prefabPath);
            return instance.GetRequiredComponent<T>();
        }

        public static string PadRichTextNumber(string number, int width, bool padOnRight = false) {
            int padWidth = width - number.Length;
            if (padWidth < 0) {
                return number;
            }
            if (padOnRight) {
                return string.Format(
                    "{0}<color=#FFFFFF00>{1}</color>",
                    number,
                    new string('0', padWidth)
                );
            } else {
                return string.Format(
                    "<color=#FFFFFF00>{0}</color>{1}",
                    new string('0', padWidth),
                    number
                );
            }
        }

        public static string ApplyRichTextColor(string text, Color color) {
            return string.Format("<color=#{0}>{1}</color>", ColorUtility.ToHtmlStringRGBA(color), text);
        }
    }
}
