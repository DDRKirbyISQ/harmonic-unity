﻿namespace HarmonicUnity
{
    public static class PlatformUtils
    {
        static BuildTarget? _platformOverride;

        public static void SetOverride(BuildTarget? platformOverride) {
            _platformOverride = platformOverride;
        }

        public static BuildTarget CurrentTarget() {
#if UNITY_IOS
            return BuildTarget.Ios;
#elif UNITY_ANDROID
            return BuildTarget.Android;
#elif UNITY_WEBGL
            return BuildTarget.WebGl;
#elif UNITY_LINUX
            return BuildTarget.Linux;
#elif UNITY_STANDALONE_WIN
            return BuildTarget.Windows;
#elif UNITY_STANDALONE_OSX
            return BuildTarget.Osx;
#elif UNITY_STANDALONE_LINUX
            return BuildTarget.Linux;
#elif UNITY_SWITCH
            return BuildTarget.Switch;
#endif
        }

        public static bool CheckBuildTarget(BuildTarget flags) {
            if (_platformOverride.HasValue) {
                return (_platformOverride & flags) != 0;
            }

            return (CurrentTarget() & flags) != 0;
        }
    }
}