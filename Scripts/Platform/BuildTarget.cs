﻿using System;

[Flags]
public enum BuildTarget
{
    WebGl = 1 << 0,
    Android = 1 << 1,
    Ios = 1 << 2,
    Windows = 1 << 3,
    Osx = 1 << 4,
    Linux = 1 << 5,
    Switch = 1 << 6,
    Mobile = Android | Ios,
    Desktop = WebGl | Windows | Osx | Linux,
    Standalone = Windows | Osx | Linux,
    All = unchecked((int)0xFFFFFFFF),
}
