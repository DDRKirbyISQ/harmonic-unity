﻿using UnityEngine;

namespace HarmonicUnity
{
    /// <summary>
    /// Conditionally disables or destroys the attached gameobject for certain platforms.
    /// </summary>
    public class PlatformDependent : MonoBehaviour
    {
        [SerializeField]
        [EnumFlags]
        BuildTarget activeTargets = BuildTarget.All;

        [SerializeField]
        bool destroy = false;

        protected virtual void Awake() {
            if (!PlatformUtils.CheckBuildTarget(activeTargets)) {
                if (destroy) {
                    DestroyImmediate(gameObject);
                } else {
                    gameObject.SetActive(false);
                }
            }
        }
    }
}