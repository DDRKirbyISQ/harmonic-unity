﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace HarmonicUnity
{
    /// <summary>
    /// 
    /// </summary>
    public class PlatformerHorizontalMovement : MonoBehaviour
    {
        public float Speed = 50.0f;

        [SerializeField] InputAction _leftAction;

        [SerializeField] InputAction _rightAction;

        PlatformerPhysics _physics;

        protected virtual void ConstructPlatformerHorizontalMovement() {
            _physics = this.GetRequiredComponent<PlatformerPhysics>();
        }

        protected virtual void Awake() {
            ConstructPlatformerHorizontalMovement();
        }

        protected virtual void Start() {
            _leftAction.Enable();
            _rightAction.Enable();
        }

        protected virtual void Update() {
            if (!Timestep.UseFixedUpdatePhysics) {
                DoUpdate();
            }
        }

        protected virtual void FixedUpdate() {
            if (Timestep.UseFixedUpdatePhysics) {
                DoUpdate();
            }
        }

        void DoUpdate() {
            bool left = _leftAction.ReadValue<float>() > 0.5f;
            bool right = _rightAction.ReadValue<float>() > 0.5f;

            if (left && !right) {
                _physics.TryMove(Direction4.Left, Speed * Time.deltaTime);
            } else if (right && !left) {
                _physics.TryMove(Direction4.Right, Speed * Time.deltaTime);
            }
        }
    }
}
