﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace HarmonicUnity
{
    [RequireComponent(typeof(PlatformerVelocity))]
    public class PlatformerJumpGravity : MonoBehaviour
    {
        [Tooltip("Gravity applied to the object, in units / sec^2.")]
        public Vector2 ButtonUpGravity = new Vector2(0.0f, -300.0f);

        [Tooltip("Gravity applied to the object, in units / sec^2.")]
        public Vector2 ButtonDownGravity = new Vector2(0.0f, -150.0f);

        [SerializeField] InputAction _jumpAction;

        PlatformerVelocity _velocity;

        public Vector2 CurrentGravity() {
            return _jumpAction.ReadValue<float>() > 0.5f ? ButtonDownGravity : ButtonUpGravity;
        }

        protected void ConstructPlatformerJumpGravity() {
            _velocity = this.GetRequiredComponent<PlatformerVelocity>();
        }

        protected virtual void Awake() {
            ConstructPlatformerJumpGravity();
        }

        protected virtual void Start() {
            _jumpAction.Enable();
        }

        protected virtual void Update() {
            if (!Timestep.UseFixedUpdatePhysics) {
                DoUpdate();
            }
        }

        protected virtual void FixedUpdate() {
            if (Timestep.UseFixedUpdatePhysics) {
                DoUpdate();
            }
        }

        void DoUpdate() {
            _velocity.Velocity += CurrentGravity() * Time.deltaTime;
        }
    }
}
