﻿using UnityEngine;
using System.Collections;

namespace HarmonicUnity
{
    public class OneWayPlatformCollidable : MonoBehaviour, ICollidable
    {
        public float AllowedMovementInto(Transform transform, Direction4 direction, float collideDistance) {
            if (direction == Direction4.Down) {
                return 0.0f;
            } else {
                return collideDistance;
            }
        }

        public void MovementInto(Transform transform, Direction4 direction, float collideDistance) {
        }

        protected void ConstructOneWayPlatformCollidable() {
        }

        protected virtual void Awake() {
            ConstructOneWayPlatformCollidable();
        }
    }
}
