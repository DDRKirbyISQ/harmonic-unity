﻿using UnityEngine;
using System.Collections;

namespace HarmonicUnity
{
    public interface ICollidable
    {
        /// <summary>
        /// Collides the into.
        /// </summary>
        /// <returns>The allowed distance to collide into the object.</returns>
        /// <param name="transform">Transform.</param>
        /// <param name="direction">Direction.</param>
        /// <param name="collideDistance">Collide distance.</param>
        float AllowedMovementInto(Transform transform, Direction4 direction, float collideDistance);

        void MovementInto(Transform transform, Direction4 direction, float collideDistance);
    }
}
