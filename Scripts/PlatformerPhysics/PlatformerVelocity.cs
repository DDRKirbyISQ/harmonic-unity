﻿using UnityEngine;
using UnityEngine.Events;
using System;
using System.Collections;

namespace HarmonicUnity
{
    [RequireComponent(typeof(PlatformerPhysics))]
    public class PlatformerVelocity : MonoBehaviour
    {
        [Tooltip("If true, Velocity gets reduced or reset when a movement-blocking collision occurs.")]
        public bool RespondToCollision = true;

        public float MinimumCollisionMagnitude = 0.01f;

        /// <summary>
        /// Current velocity, in units / sec.
        /// </summary>
        public Vector2 Velocity = new Vector2(0.0f, 0.0f);

        public UnityEvent CollisionX;

        public UnityEvent CollisionY;

        PlatformerPhysics _physics;

        protected void ConstructPlatformerVelocity() {
            _physics = this.GetRequiredComponent<PlatformerPhysics>();
        }

        protected virtual void Awake() {
            ConstructPlatformerVelocity();
        }

        public virtual void Update() {
            if (!Timestep.UseFixedUpdatePhysics) {
                DoUpdate();
            }
        }

        public virtual void FixedUpdate() {
            if (Timestep.UseFixedUpdatePhysics) {
                DoUpdate();
            }
        }

        void DoUpdate() {
            float xMovement = _physics.TryMoveX(Velocity.x * Time.deltaTime);
            float yMovement = _physics.TryMoveY(Velocity.y * Time.deltaTime);
            if (RespondToCollision && Time.deltaTime > 0.0f) {
                if (Mathf.Abs(xMovement / Time.deltaTime - Velocity.x) >= MinimumCollisionMagnitude) {
                    Velocity.x = xMovement / Time.deltaTime;
                    CollisionX.Invoke();
                }
                if (Mathf.Abs(yMovement / Time.deltaTime - Velocity.y) >= MinimumCollisionMagnitude) {
                    Velocity.y = yMovement / Time.deltaTime;
                    CollisionY.Invoke();
                }
            }
        }
    }
}
