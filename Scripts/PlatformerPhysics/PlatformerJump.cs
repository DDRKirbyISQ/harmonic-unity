﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace HarmonicUnity
{
    /// <summary>
    /// TODO
    /// </summary>
    [RequireComponent(typeof(PlatformerVelocity))]
    [RequireComponent(typeof(PlatformerPhysics))]
    public class PlatformerJump : MonoBehaviour
    {
        public float Speed = 75.0f;

        public int JumpCount = 1;

        public IntEvent JumpEvent;

        [SerializeField] InputAction _jumpAction;

        PlatformerVelocity _velocity;

        PlatformerPhysics _physics;

        int _currentJump = 1;

        protected virtual void ConstructPlatformerJump() {
            _velocity = this.GetRequiredComponent<PlatformerVelocity>();
            _physics = this.GetRequiredComponent<PlatformerPhysics>();
        }

        protected virtual void Awake() {
            ConstructPlatformerJump();
        }

        protected virtual void Start() {
            _jumpAction.Enable();
        }

        protected virtual void Update() {
            if (!Timestep.UseFixedUpdatePhysics) {
                DoUpdate();
            }

            if (_jumpAction.triggered && CanJump()) {
                Jump();
            }
        }

        protected virtual void FixedUpdate() {
            if (Timestep.UseFixedUpdatePhysics) {
                DoUpdate();
            }
        }

        void DoUpdate() {
            if (_physics.IsGrounded() && _velocity.Velocity.y <= 0.0f) {
                _currentJump = 0;
            }
        }

        bool CanJump() {
            return _currentJump < JumpCount;
        }

        void Jump() {
            _velocity.Velocity.y = Speed;
            ++_currentJump;
            JumpEvent.Invoke(_currentJump);
        }
    }
}
