﻿using UnityEngine;
using System.Collections;

namespace HarmonicUnity
{
    [RequireComponent(typeof(PlatformerPhysics))]
    public class MovementTransferCollidable : MonoBehaviour, ICollidable
    {
        PlatformerPhysics _physics;

        public float AllowedMovementInto(Transform transform, Direction4 direction, float collideDistance) {
            return _physics.AllowedMove(direction, collideDistance);
        }

        public void MovementInto(Transform transform, Direction4 direction, float collideDistance) {
            _physics.ForceMove(direction, collideDistance);
        }

        protected void ConstructMovementTransferCollidable() {
            _physics = this.GetRequiredComponent<PlatformerPhysics>();
        }

        protected virtual void Awake() {
            ConstructMovementTransferCollidable();
        }
    }
}
