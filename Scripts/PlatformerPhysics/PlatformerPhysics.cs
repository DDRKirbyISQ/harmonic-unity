﻿using UnityEngine;
using System.Collections.Generic;

namespace HarmonicUnity
{
    /// <summary>
    /// A component that handles 2D platforming movement for the attached GameObject.
    /// 
    /// Usage:
    ///     // TODO
    /// </summary>
    public class PlatformerPhysics : MonoBehaviour
    {
        const int kMaxRaycastHits = 25;

        /// <summary>
        /// If set to true, purple debug lines are drawn whenever collision raycasts are performed.
        /// Only works if Gizmos are enabled for the current view!
        /// </summary>
        public static bool DrawDebugRays = true;

        [Tooltip("Layers that this object will collide with.")]
        public LayerMask CollisionMask;

        /// <summary>
        /// Reusable array to reduce allocations.
        /// </summary>
        static readonly RaycastHit2D[] _raycastHits = new RaycastHit2D[kMaxRaycastHits];

        /// <summary>
        /// Reusable list to reduce allocations.
        /// </summary>
        static readonly List<RaycastHit2D> _raycastHitsForMovement = new List<RaycastHit2D>(50);

        /// <summary>
        /// Reusable dictionary to reduce allocations.
        /// </summary>
        static readonly Dictionary<Transform, RaycastHit2D> _uniqueHits = new Dictionary<Transform, RaycastHit2D>();

        [SerializeField]
        float _slopeTolerance = 1.0f;

        [SerializeField]
        float _raycastMargin = 0.01f;

        Collider2D _collider;

        public float TryMove(Direction4 direction, float distance) {
            if (distance == 0.0f) {
                return 0.0f;
            } else if (distance < 0.0f) {
                Debug.LogWarningFormat("TryMove with negative distance {0}; inverting...", distance);
                distance = -distance;
                direction = direction.Opposite();
            }

            float allowedMove = AllowedMove(direction, distance);
            ForceMove(direction, allowedMove);

            float remainingDistance = distance - allowedMove;
            // Check for slopes.
            if (_slopeTolerance > 0.0f && direction.IsHorizontal()) {
                if (remainingDistance > 0.0f) {
                    // We hit an obstacle, so check for upwards sloping.
                    // TODO: This is a little hacky right now.

                    // Store our original position in case we need to back out.
                    Vector3 originalPosition = transform.position;

                    // Try moving up, up to remainingDistance * SlopeTolerance.
                    // Add float.Epsilon to get up slightly higher.
                    float maxUpwardMove = remainingDistance * _slopeTolerance + float.Epsilon;
                    float allowedUpwardMove = AllowedMove(Direction4.Up, maxUpwardMove);
                    ForceMove(Direction4.Up, allowedUpwardMove);

                    // Now try moving right the remaining distance.
                    float allowedHorizontalMove = AllowedMove(direction, remainingDistance);
                    ForceMove(direction, allowedHorizontalMove);

                    // Now try finding the ground again.
                    float allowedMinY = originalPosition.y - maxUpwardMove;
                    float maxDownDist = transform.position.y - allowedMinY;
                    float allowedDownwardMove = AllowedMove(Direction4.Down, maxDownDist);
                    if (allowedDownwardMove < maxDownDist) {
                        // We hit the ground.  Success!  Move downwards to hit the ground.
                        ForceMove(Direction4.Down, allowedDownwardMove);
                    } else {
                        // We didn't hit the ground.  Reset our position.
                        transform.position = originalPosition;
                    }
                } else {
                    // We moved successfully, so try to stick to downwards slopes.

                    // Try moving downwards by distance * SlopeTolerance.
                    // If we hit a floor, stick to it.
                    float maxDownwardMove = distance * _slopeTolerance + float.Epsilon;
                    float allowedDownwardMove = AllowedMove(Direction4.Down, maxDownwardMove);
                    if (allowedDownwardMove < maxDownwardMove) {
                        ForceMove(Direction4.Down, allowedDownwardMove);
                    }
                }
            }

            return allowedMove;
        }

        public Vector2 TryMoveTo(Vector2 position) {
            Vector2 delta = new Vector2(position.x - transform.position.x, position.y - transform.position.y);
            return TryMove(delta);
        }

        public Vector2 TryMove(Vector2 delta) {
            float x = TryMoveX(delta.x);
            float y = TryMoveY(delta.y);
            return new Vector2(x, y);
        }

        public float TryMoveX(float delta) {
            Direction4 direction = delta >= 0.0f ? Direction4.Right : Direction4.Left;
            float distance = TryMove(direction, Mathf.Abs(delta));
            return Mathf.Sign(delta) * distance;
        }

        public float TryMoveY(float delta) {
            Direction4 direction = delta >= 0.0f ? Direction4.Up : Direction4.Down;
            float distance = TryMove(direction, Mathf.Abs(delta));
            return Mathf.Sign(delta) * distance;
        }

        public bool CanMoveInDirection(Direction4 direction) {
            return AllowedMove(direction, 0.02f) > 0.01f;
        }

        public bool IsGrounded() {
            return !CanMoveInDirection(Direction4.Down);
        }

        public float AllowedMove(Direction4 direction, float distance) {
            if (distance == 0.0f) {
                return 0.0f;
            } else if (distance < 0.0f) {
                Debug.LogWarningFormat("AllowedMove with negative distance {0}; inverting...", distance);
                distance = -distance;
                direction = direction.Opposite();
            }

            _raycastHitsForMovement.Clear();
            RaycastHitsForMovement(_raycastHitsForMovement, direction, distance);

            foreach (RaycastHit2D raycastHit in _raycastHitsForMovement) {
                // If another object already reduced our allowed distance to before this raycast hit, exit out.
                if (distance <= raycastHit.distance) {
                    break;
                }

                // Check for an ICollidable implementation that handles colliding with this object.
                ICollidable collidable;
                if (!raycastHit.transform.TryGetComponent<ICollidable>(out collidable)) {
                    // Use a default implementation if none exists on the object.
                    collidable = DefaultCollidable.Get();
                }

                float collideDistance = Mathf.Max(0.0f, distance - raycastHit.distance);
                float allowedCollideDistance = collidable.AllowedMovementInto(transform, direction, collideDistance);
                float allowedDistance = allowedCollideDistance + raycastHit.distance;
                if (allowedDistance < distance) {
                    distance = allowedDistance;
                }
            }

            return distance;
        }


        public void ForceMove(Direction4 direction, float distance) {
            if (distance == 0.0f) {
                return;
            } else if (distance < 0.0f) {
                Debug.LogWarningFormat("Move with negative distance {0}; inverting...", distance);
                distance = -distance;
                direction = direction.Opposite();
            }

            _raycastHitsForMovement.Clear();
            RaycastHitsForMovement(_raycastHitsForMovement, direction, distance);
            foreach (RaycastHit2D raycastHit in _raycastHitsForMovement) {
                // Check for an ICollidable implementation that handles colliding with this object.
                ICollidable collidable;
                if (!raycastHit.transform.TryGetComponent<ICollidable>(out collidable)) {
                    // Use a default implementation if none exists on the object.
                    collidable = DefaultCollidable.Get();
                }

                float collideDistance = Mathf.Max(0.0f, distance - raycastHit.distance);
                collidable.MovementInto(transform, direction, collideDistance);
            }

            // Actually move.
            transform.position += (Vector3)direction.ToVector2(distance);
        }

        /// <summary>
        /// Returns the set of all raycast hits that would result from the movement of a 2D box in a cardinal direction.
        /// Results are sorted by distance.
        /// </summary>
        /// <param name="results">List into which to write resulting RaycastHit2D hits for the movement.</param>
        /// <param name="direction">Direction of movement.</param>
        /// <param name="distance">Distance of movement in worldspace units.</param>
        public void RaycastHitsForMovement(
            List<RaycastHit2D> results,
            Direction4 direction,
            float distance
        ) {
            // TODO: Do we need to add a little bit to distance?
            if (distance == 0.0f) {
                return;
            } else if (distance < 0.0f) {
                Debug.LogWarningFormat("RaycastHitsForMovement with negative distance {0}; inverting...", distance);
                distance = -distance;
                direction = direction.Opposite();
            }

            _uniqueHits.Clear();
            Bounds bounds = _collider.bounds;
            // Factor in raycast margin.
            Vector2 boundsSize = bounds.size;
            if (direction == Direction4.Left || direction == Direction4.Right) {
                boundsSize.y -= _raycastMargin * 2;
            } else {
                boundsSize.x -= _raycastMargin * 2;
            }
            bounds.size = boundsSize;

            int raycastHitCount = Physics2D.BoxCastNonAlloc(
                bounds.center,
                bounds.size,
                0.0f,
                direction.ToVector2(),
                _raycastHits,
                distance,
                CollisionMask
            );

            if (DrawDebugRays) {
                if (direction == Direction4.Left || direction == Direction4.Right) {
                    Vector2 originTop = (Vector2)bounds.center + Vector2.up * bounds.extents.y;
                    Vector2 originBottom = (Vector2)bounds.center - Vector2.up * bounds.extents.y;
                    Debug.DrawLine(originTop, originTop + direction.ToVector2(distance + bounds.extents.x), Color.magenta);
                    Debug.DrawLine(originBottom, originBottom + direction.ToVector2(distance + bounds.extents.x), Color.magenta);
                    Debug.DrawLine(bounds.center, (Vector2)bounds.center + direction.ToVector2(distance + bounds.extents.x), Color.red);
                } else {
                    Vector2 originRight = (Vector2)bounds.center + Vector2.right * bounds.extents.x;
                    Vector2 originLeft = (Vector2)bounds.center - Vector2.right * bounds.extents.x;
                    Debug.DrawLine(originRight, originRight + direction.ToVector2(distance + bounds.extents.y), Color.magenta);
                    Debug.DrawLine(originLeft, originLeft + direction.ToVector2(distance + bounds.extents.y), Color.magenta);
                    Debug.DrawLine(bounds.center, (Vector2)bounds.center + direction.ToVector2(distance + bounds.extents.y), Color.red);
                }
            }

            for (int i = 0; i < raycastHitCount; ++i) {
                RaycastHit2D raycastHit = _raycastHits[i];
                // Filter out hits against the object itself.
                if (raycastHit.transform == transform) {
                    continue;
                }

                // Filter out hits that are before the center of the collider.
                if (direction == Direction4.Left) {
                    if (raycastHit.point.x > bounds.center.x) {
                        continue;
                    }
                } else if (direction == Direction4.Down) {
                    if (raycastHit.point.y > bounds.center.y) {
                        continue;
                    }
                } else if (direction == Direction4.Up) {
                    if (raycastHit.point.y < bounds.center.y) {
                        continue;
                    }
                } else if (direction == Direction4.Right) {
                    if (raycastHit.point.x < bounds.center.x) {
                        continue;
                    }
                }

                if (!_uniqueHits.ContainsKey(raycastHit.transform)) {
                    _uniqueHits.Add(raycastHit.transform, raycastHit);
                } else if (_uniqueHits[raycastHit.transform].distance > raycastHit.distance) {
                    _uniqueHits[raycastHit.transform] = raycastHit;
                }
            }
            results.AddRange(_uniqueHits.Values);
        }

        protected void ConstructPlatformerMovement() {
            _collider = this.GetRequiredComponent<Collider2D>();
        }

        protected virtual void Awake() {
            ConstructPlatformerMovement();
        }
    }
}
