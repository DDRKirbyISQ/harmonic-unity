﻿using UnityEngine;

namespace HarmonicUnity
{
    [RequireComponent(typeof(PlatformerVelocity))]
    public class PlatformerConstantGravity : MonoBehaviour
    {
        [Tooltip("Gravity applied to the object, in units / sec^2.")]
        public Vector2 Gravity = new Vector2(0.0f, -300.0f);

        PlatformerVelocity _velocity;

        protected void ConstructPlatformerConstantGravity() {
            _velocity = this.GetRequiredComponent<PlatformerVelocity>();
        }

        protected virtual void Awake() {
            ConstructPlatformerConstantGravity();
        }

        protected virtual void Update() {
            if (!Timestep.UseFixedUpdatePhysics) {
                DoUpdate();
            }
        }

        protected virtual void FixedUpdate() {
            if (Timestep.UseFixedUpdatePhysics) {
                DoUpdate();
            }
        }

        void DoUpdate() {
            _velocity.Velocity += Gravity * Time.deltaTime;
        }
    }
}
