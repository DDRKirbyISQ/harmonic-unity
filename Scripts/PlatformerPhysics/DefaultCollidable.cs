﻿using UnityEngine;

namespace HarmonicUnity
{
    /// <summary>
    /// A default collidable implementation used for objects that have no specific ICollidable implementation.
    /// Does not allow any movement into the object.
    /// </summary>
    public class DefaultCollidable : ICollidable
    {
        static DefaultCollidable _instance = new DefaultCollidable();

        private DefaultCollidable() {
        }

        /// <summary>
        /// Returns a shared DefaultCollidable instance.
        /// </summary>
        public static DefaultCollidable Get() {
            return _instance;
        }

        public float AllowedMovementInto(Transform transform, Direction4 direction, float collideDistance) {
            return 0.0f;
        }

        public void MovementInto(Transform transform, Direction4 direction, float collideDistance) {
        }
    }
}
