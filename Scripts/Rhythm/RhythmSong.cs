﻿using UnityEngine;
using UnityEngine.Assertions;

namespace HarmonicUnity
{
    /// <summary>
    /// Note: This is probably suboptimal.
    /// You should use AudioSettings.dspTime instead if possible.
    /// </summary>
    public class RhythmSong
    {
        public float BeatsPerMinute { get; protected set; }

        public float BeatsPerSecond { get; protected set; }

        public float SecondsPerBeat { get; protected set; }

        AudioSource _audioSource;

        /// <summary>
        /// 1 / sample frequency.
        /// </summary>
        float _inverseFrequency;

        float _audioLatency;

        float _lastUpdateTime = -1.0f;

        float _currentTime = 0.0f;

        float _previousTime = -1.0f;

        /// <summary>
        /// Set to true when we are manipulating audioSource pitch.
        /// </summary>
        bool _debugMode = false;

        public RhythmSong(AudioSource audioSource, float beatsPerMinute) {
#if UNITY_EDITOR
            Assert.IsNotNull(
                audioSource.clip,
                string.Format("Missing AudioClip for {0} - must have a clip loaded to create a RhythmSong!", audioSource)
            );
#endif

            _audioSource = audioSource;
            BeatsPerMinute = beatsPerMinute;
            BeatsPerSecond = beatsPerMinute / 60.0f;
            SecondsPerBeat = 60.0f / beatsPerMinute;
            _inverseFrequency = 1.0f / audioSource.clip.frequency;

            int bufferLength;
            int numBuffers;
            AudioSettings.GetDSPBufferSize(out bufferLength, out numBuffers);

            // TODO: Looks like we don't actually need this.
            _audioLatency = (float)(bufferLength * numBuffers) / AudioSettings.outputSampleRate;
            _audioLatency = 0.0f;
        }

        /// <summary>
        /// Fades the song.
        /// </summary>
        /// <param name="duration">
        /// Speed of fade, in amount per second.
        /// Defaults to AudioSourceFader.kDefaultSpeed.
        /// </param>
        public void Fade(float speed = AudioSourceFader.kDefaultSpeed) {
            // No-op if music is not playing.
            if (!_audioSource.isPlaying) {
                Debug.LogWarningFormat("Ignoring Fade with speed {0}: Song is not playing!", speed);
                return;
            }

            AudioSourceFader.Fade(_audioSource, 0.0f, speed, StopMusic);
        }

        /// <summary>
        /// Immediately stops the song.
        /// </summary>
        public void StopMusic() {
            _audioSource.Stop();
        }

        public void ToggleDebugMode(bool debug) {
            _debugMode = debug;
        }

        public float CurrentTime() {
            if (_lastUpdateTime != Time.unscaledTime) {
                Update();
            }

            return _currentTime;
        }

        void Update() {
            _lastUpdateTime = Time.unscaledTime;

            float audioTime = (float)_audioSource.timeSamples * _inverseFrequency - _audioLatency;
            if (_debugMode) {
                _currentTime = audioTime;
                _previousTime = _currentTime;
                return;
            }

            if (!_audioSource.isPlaying) {
                return;
            }

            _previousTime = _currentTime;

            // TODO: Improve this.
            _currentTime += Time.deltaTime;
            if (Mathf.Abs(_currentTime - audioTime) > 0.05f || _debugMode) {
                _currentTime = audioTime;
            }
        }

        public bool Beat() {
            float previousBeat = TimeToBeat(_previousTime);
            float currentBeat = CurrentBeat();
            return Mathf.FloorToInt(previousBeat) != Mathf.FloorToInt(currentBeat);
        }

        public float CurrentBeat() {
            return TimeToBeat(CurrentTime());
        }

        public float TimeToBeat(float time) {
            return time * BeatsPerSecond;
        }

        public float BeatToTime(float beat) {
            return beat * SecondsPerBeat;
        }
    }
}
