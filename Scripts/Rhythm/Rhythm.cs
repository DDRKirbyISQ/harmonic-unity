﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace HarmonicUnity
{
    /// <summary>
    /// Singleton class for handling rhythm-related functionality.
    /// Currently only supports a single song.
    /// 
    /// Usage:
    /// </summary>
    public class RhythmManager : MonoBehaviour
    {

    }
}
