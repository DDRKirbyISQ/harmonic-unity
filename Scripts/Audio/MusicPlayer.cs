﻿using UnityEngine;
using HarmonicUnity;

public class MusicPlayer : MonoBehaviour
{
    [SerializeField]
    AudioClip _clip;

    [SerializeField]
    bool _looping = true;

    protected virtual void Start() {
        AudioManager.PlayMusic(_clip, _looping);
    }
}
