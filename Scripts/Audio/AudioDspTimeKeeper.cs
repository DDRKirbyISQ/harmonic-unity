﻿using UnityEngine;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace HarmonicUnity
{
    /// <summary>
    /// A rhythm game time keeping class that uses a linear regression between Time.realtimeSinceStartup
    /// and AudioSettings.dspTime to achieve a smoothed AudioSettings.dspTime value for the current frame.
    /// We use this instead of AudioSettings.dspTime because AudioSettings.dspTime updates in discrete
    /// steps that are out of sync with frame timing, so using AudioSettings.dspTime would result in
    /// inconsistent time deltas.
    /// Using AudioDspTimeKeeper.CurrentTime() gives a timing value that is based on audio dsp time but
    /// still updates smoothly and thus can be used for visual positioning (and everything else).
    /// It is also guaranteed to be monotonically increasing.
    /// </summary>
    public class AudioDspTimeKeeper : MonoBehaviour
    {
        /// <summary>
        /// Number of samples to keep for the rolling average.
        /// </summary>
        const int kCount = 15;

        static AudioDspTimeKeeper _instance {
            get { return SingletonBehavior<AudioDspTimeKeeper>.Get(); }
        }

        /// <summary>
        /// Tracked frame times from Time.realtimeSinceStartup.
        /// </summary>
        List<double> _frameTimes = new List<double>(kCount);

        /// <summary>
        /// Tracked audio times from AudioSettings.dspTime.
        /// </summary>
        List<double> _audioTimes = new List<double>(kCount);

        /// <summary>
        /// Last audio time that we returned.
        /// Used to ensure that we are monotonically increasing.
        /// </summary>
        double _lastAudioTime = 0.0f;

        double _coeff0;

        double _coeff1;

        public static void Init() {
            SingletonBehavior<AudioDspTimeKeeper>.Get();
        }

        public static double AudioTimeAtRealtime(double time) {
            return time * _instance._coeff1 + _instance._coeff0;
        }

        public static float RealtimeAtAudioTime(float time) {
            return (float)((time - _instance._coeff0) / _instance._coeff1);
        }

        /// <summary>
        /// Returns the current tracked audio time (a smoothed version of AudioSettings.dspTime).
        /// Guaranteed to be monotonically increasing.
        /// </summary>
        public static double CurrentAudioTime() {
            double result = AudioSettings.dspTime;
            if (_instance._frameTimes.Count >= kCount) {
                result = Time.realtimeSinceStartupAsDouble * _instance._coeff1 + _instance._coeff0;
            }
            // Make sure we are monotonically increasing no matter what.
            if (result > _instance._lastAudioTime) {
                _instance._lastAudioTime = result;
            }
            return _instance._lastAudioTime;
        }

        protected virtual void Start() {
#if UNITY_EDITOR
            EditorApplication.pauseStateChanged += OnEditorPause;
#endif
#if UNITY_SWITCH
            UnityEngine.Switch.Notification.notificationMessageReceived += SwitchNotification;
#endif

            for (int i = 0; i < kCount; ++i) {
                _frameTimes.Add(Time.realtimeSinceStartupAsDouble);
                _audioTimes.Add(AudioSettings.dspTime);
            }
            UpdateRegression();
        }

        protected virtual void OnDestroy() {
#if UNITY_EDITOR
            EditorApplication.pauseStateChanged -= OnEditorPause;
#endif
#if UNITY_SWITCH
            UnityEngine.Switch.Notification.notificationMessageReceived -= SwitchNotification;
#endif
        }

#if UNITY_SWITCH
        void SwitchNotification(UnityEngine.Switch.Notification.Message message) {
            if (message == UnityEngine.Switch.Notification.Message.Resume) {
                OnApplicationPause(false);
            }
        }
#endif

#if UNITY_EDITOR
        void OnEditorPause(PauseState state) {
            OnApplicationPause(state == PauseState.Paused);
        }
#endif

        protected virtual void Update() {
            if (_frameTimes.Count >= kCount) {
                _frameTimes.RemoveAt(0);
            }
            _frameTimes.Add(Time.realtimeSinceStartupAsDouble);
            if (_audioTimes.Count >= kCount) {
                _audioTimes.RemoveAt(0);
            }
            _audioTimes.Add(AudioSettings.dspTime);
            UpdateRegression();
        }

        protected virtual void OnApplicationPause(bool paused) {
            // When the app pauses, realtime continues to advance even though DSP time doesn't.
            // Reset all data points and wait for more samples.
            _frameTimes.Clear();
            _audioTimes.Clear();
            UpdateRegression();
        }

        void UpdateRegression() {
            if (_frameTimes.Count < kCount) {
                // Not enough data yet.
                _coeff0 = AudioSettings.dspTime - Time.realtimeSinceStartupAsDouble;
                _coeff1 = 1;
                return;
            }

            // Use simple linear regression...
            double frameTimeSum = 0.0f;
            foreach (double time in _instance._frameTimes) {
                frameTimeSum += time;
            }
            double audioTimeSum = 0.0f;
            foreach (double time in _instance._audioTimes) {
                audioTimeSum += time;
            }
            double averageTime = frameTimeSum / _instance._frameTimes.Count;
            double averageDspTime = audioTimeSum / _instance._audioTimes.Count;
            double variance = 0.0f;
            double covariance = 0.0f;
            for (int i = 0; i < kCount; ++i) {
                variance += (_instance._frameTimes[i] - averageTime) * (_instance._frameTimes[i] - averageTime);
                covariance += (_instance._frameTimes[i] - averageTime) * (_instance._audioTimes[i] - averageDspTime);
            }

            if (variance == 0.0f) {
                _coeff1 = 1.0f;
            } else {
                _coeff1 = covariance / variance;
            }
            _coeff0 = averageDspTime - _coeff1 * averageTime;
            // Now realtime * _coeff1 + _coeff0 should equal musictime
        }
    }
}