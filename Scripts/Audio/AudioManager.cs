﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Assertions;
using UnityEngine.AddressableAssets;
using System.Collections;
using System.Collections.Generic;

namespace HarmonicUnity
{
    /// <summary>
    /// Singleton class for handling basic audio-related functionality.
    /// 
    /// Audio clips with the Sfx tag are all cached when this initializes, so they don't have to be loaded
    /// individually.
    /// 
    /// Usage:
    ///     // Play an addressable sound under the "Sfx" addressable tag.
    ///     AudioManager.PlaySound("Sfx/MySound");
    ///
    ///     // Play an addressable music clip.
    ///     AudioManager.PlayMusic("Assets/Music/MyMusic");
    /// 
    ///     // Stop music.
    ///     AudioManager.StopMusic();
    /// 
    ///     // Fade out music.
    ///     AudioManager.FadeMusic();
    /// </summary>
    public class AudioManager : MonoBehaviour
    {
        /// <summary>
        /// Returns the static AudioSource used for playing music.
        /// </summary>
        public static AudioSource MusicSource { get { return _instance._musicSource; } }

        static AudioManager _instance {
            get { return SingletonBehavior<AudioManager>.Get("HarmonicUnity/AudioManager"); }
        }

        /// <summary>
        /// AudioSource used to play music.
        /// </summary>
        [SerializeField]
        AudioSource _musicSource;

        public static AudioMixerGroup SfxGroup { get { return _instance._sfxGroup; } }

        public static AudioMixerGroup MusicGroup { get { return _instance._musicGroup; } }

        [SerializeField]
        AudioMixerGroup _sfxGroup;

        [SerializeField]
        AudioMixerGroup _musicGroup;

        /// <summary>
        /// Silent audio clip.  Played on init to prevent Unity's hiccup on initial audio play.
        /// </summary>
        [SerializeField]
        AudioClip _silenceClip;

        HashSet<int> _played = new HashSet<int>();

        Dictionary<string, AudioClip> _cachedClips;

        bool _initialized = false;

#if UNITY_WEBGL
        List<(double, AudioSource)> _scheduledSounds = new List<(double, AudioSource)>(20);

        class ScheduledSoundsComparer : IComparer<(double, AudioSource)>
        {
            public int Compare((double, AudioSource) a, (double, AudioSource) b) {
                return a.Item1.CompareTo(b.Item1);
            }
        }

        ScheduledSoundsComparer kScheduledSoundsComparer = new ScheduledSoundsComparer();
#endif

        public static void Init() {
            SingletonBehavior<AudioManager>.Get("HarmonicUnity/AudioManager");
        }

        public static bool Initialized { get { return _instance._initialized; } }

        public static AudioSource CreateSource(string audioClipPath, float volume = 1.0f) {
            AudioClip clip = LoadClipFromPath(audioClipPath);
            Assert.IsNotNull(clip);
            if (clip == null) {
                return null;
            }

            if (_instance._played.Contains(clip.GetHashCode())) {
                return null;
            }
            _instance._played.Add(clip.GetHashCode());

            _instance.PurgeSources();
            AudioSource source = _instance.gameObject.AddComponent<AudioSource>();
            source.volume = volume;
            source.clip = clip;
            source.outputAudioMixerGroup = SfxGroup;
            return source;
        }

        /// <summary>
        /// Plays a one-shot sound.
        /// Only allows one instance of the same sound to be played per frame.
        /// TODO: Allow customizeable volume stacking behavior when this happens.
        /// </summary>
        /// <returns>AudioSource used to play the sound, or null if no sound was played.</returns>
        /// <param name="audioClipPath">Path to audio clip, under Sfx/.</param>
        public static AudioSource PlaySound(string audioClipPath, float volume = 1.0f) {
            AudioClip clip = LoadClipFromPath(audioClipPath);
            return PlaySound(clip, volume);
        }

        /// <summary>
        /// Plays a one-shot sound.
        /// Only allows one instance of the same sound to be played per frame.
        /// TODO: Allow customizeable volume stacking behavior when this happens.
        /// </summary>
        /// <returns>AudioSource used to play the sound, or null if no sound was played.</returns>
        /// <param name="audioClipPath">Audio clip to play.</param>
        public static AudioSource PlaySound(AudioClip clip, float volume = 1.0f) {
            Assert.IsNotNull(clip);
            if (clip == null) {
                return null;
            }

            if (_instance._played.Contains(clip.GetHashCode())) {
                return null;
            }
            _instance._played.Add(clip.GetHashCode());

            _instance.PurgeSources();
            AudioSource source = _instance.gameObject.AddComponent<AudioSource>();
            source.volume = volume;
            source.PlayOneShot(clip);
            source.outputAudioMixerGroup = SfxGroup;
            return source;
        }

        public static AudioSource PlayScheduled(string audioClipPath, double dspTime, float volume = 1.0f) {
            AudioClip clip = LoadClipFromPath(audioClipPath);
            return PlayScheduled(clip, dspTime, volume);
        }

        public static AudioSource PlayScheduled(AudioClip clip, double dspTime, float volume = 1.0f) {
            _instance.PurgeSources();
            AudioSource source = _instance.gameObject.AddComponent<AudioSource>();
            source.clip = clip;
            source.volume = volume;
            PlayScheduledHelper(source, dspTime);
            source.outputAudioMixerGroup = SfxGroup;
            return source;
        }

        /// <summary>
        /// Plays and loops a music clip, immediately stopping any previous clip that was playing.
        /// Does nothing if the clip was already playing.
        /// </summary>
        /// <param name="audioClipPath">Addressable path to audio clip.</param>
        /// <param name="loop">Whether to loop music.  Defaults to true.</param>
        public static void PlayMusic(string audioClipPath, bool loop = true) {
            AudioClip clip = LoadClipFromPath(audioClipPath);
            PlayMusic(clip, loop);
        }

        /// <summary>
        /// Plays and loops a music clip, immediately stopping any previous clip that was playing.
        /// Does nothing if the clip was already playing.
        /// </summary>
        /// <param name="audioClip">AudioClip to play.</param>
        /// <param name="loop">Whether to loop music.  Defaults to true.</param>
        public static void PlayMusic(AudioClip audioClip, bool loop = true) {
            if (_instance._musicSource.clip == audioClip && _instance._musicSource.isPlaying) {
                return;
            }

            _instance._musicSource.clip = audioClip;
            _instance._musicSource.Play();
            _instance._musicSource.volume = 1.0f;
            _instance._musicSource.loop = loop;
        }

        /// <summary>
        /// Pauses the current music clip, if there is one.
        /// </summary>
        public static void PauseMusic() {
            // No-op if music is not playing.
            if (!IsMusicPlaying()) {
                Debug.LogWarningFormat("Ignoring PauseMusic: Music is not playing!");
                return;
            }

            _instance._musicSource.Pause();
        }

        /// <summary>
        /// Unpauses the current music clip, if there is one.
        /// </summary>
        public static void UnpauseMusic() {
            // TODO: Check that there is a music clip?

            _instance._musicSource.UnPause();
        }

        /// <summary>
        /// Whether a music clip is currently playing.
        /// </summary>
        /// <returns>True if music is playing, false otherwise.</returns>
        public static bool IsMusicPlaying() {
            return _instance._musicSource.isPlaying;
        }

        /// <summary>
        /// Fades the current music clip, if there is one.
        /// </summary>
        /// <param name="duration">
        /// Speed of fade, in amount per second.
        /// Defaults to AudioSourceFader.kDefaultSpeed.
        /// </param>
        public static void FadeMusic(float speed = AudioSourceFader.kDefaultSpeed) {
            // No-op if music is not playing.
            if (!IsMusicPlaying()) {
                Debug.LogWarningFormat("Ignoring FadeMusic with speed {0}: Music is not playing!", speed);
                return;
            }

            AudioSourceFader.Fade(MusicSource, 0.0f, speed, StopMusic);
        }

        /// <summary>
        /// Immediately stops the current music clip, if there is one.
        /// </summary>
        public static void StopMusic() {
            _instance._musicSource.Stop();
            _instance._musicSource.clip = null;
        }

        public static void PlayScheduledHelper(AudioSource source, double time) {
            StopHelper(source);
#if UNITY_WEBGL
            // Correct for buffer time.
            AudioConfiguration config = AudioSettings.GetConfiguration();
            time -= config.dspBufferSize / config.sampleRate;
            if (time <= AudioSettings.dspTime) {
                source.Play();
            } else {
                _instance._scheduledSounds.Add((time, source));
            }
#else
            source.PlayScheduled(time);
#endif
        }

        public static void StopHelper(AudioSource source) {
            source.Stop();
#if UNITY_WEBGL
            _instance._scheduledSounds.RemoveAll((scheduled) => scheduled.Item2 == source);
#endif
        }

        protected virtual void Awake() {
            _musicSource.outputAudioMixerGroup = _musicGroup;
        }

        protected virtual IEnumerator Start() {
            PlaySound(_silenceClip, 0.0f);
            // Preload all Sfx addressable audioclips.
            // We keep this reference forever, no need to unload it.
            var handle = Addressables.LoadAssetsAsync<AudioClip>("Sfx", null);
            yield return handle;
            IList<AudioClip> clips = handle.Result;
            _cachedClips = new Dictionary<string, AudioClip>(clips.Count);
            foreach (AudioClip clip in clips) {
                _cachedClips.Add(string.Format("Sfx/{0}", clip.name), clip);
                clip.LoadAudioData();
            }
            _initialized = true;
        }

        protected virtual void Update() {
            _played.Clear();
#if UNITY_WEBGL
            _scheduledSounds.Sort(kScheduledSoundsComparer);
            int i = 0;
            double audioTime = AudioDspTimeKeeper.AudioTimeAtRealtime(Time.realtimeSinceStartupAsDouble);
            if (AudioSettings.dspTime > audioTime) {
                audioTime = AudioSettings.dspTime;
            }
            while (i < _scheduledSounds.Count) {
                if (_scheduledSounds[i].Item1 > audioTime) {
                    break;
                }
                if (_scheduledSounds[i].Item2 != null) {
                    _scheduledSounds[i].Item2.Play();
                }
                ++i;
            }
            _scheduledSounds.RemoveRange(0, i);
#endif
        }

        protected virtual void OnGUI() {
            Update();
        }

        public static AudioClip LoadClipFromPath(string path) {
            AudioClip result;
            if (!_instance._cachedClips.TryGetValue(path, out result)) {
                Debug.LogErrorFormat("Loading non-cached audioclip {0} by name", path);
                return Addressables.LoadAssetAsync<AudioClip>(path).WaitForCompletion();
            }
            return result;
        }

        void PurgeSources() {
            AudioSource[] sources = GetComponents<AudioSource>();
            foreach (AudioSource source in sources) {
                if (source == _musicSource) {
                    continue;
                }

#if UNITY_WEBGL
                if (_scheduledSounds.Exists((scheduledSound) => scheduledSound.Item2 == source)) {
                    continue;
                }
#endif

                if (!source.isPlaying) {
                    Destroy(source);
                }
            }
        }
    }
}
