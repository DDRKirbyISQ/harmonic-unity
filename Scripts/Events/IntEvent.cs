﻿using UnityEngine.Events;

/// <summary>
/// Generic int event type.
/// </summary>
[System.Serializable]
public class IntEvent : UnityEvent<int>
{
}
