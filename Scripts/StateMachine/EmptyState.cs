﻿namespace HarmonicUnity
{
    /// <summary>
    /// A default starting state for StateMachine classes that does nothing.
    /// </summary>
    public class EmptyState : StateMachineState
    {
        static EmptyState _instance = new EmptyState();

        /// <summary>
        /// Returns a shared EmptyState instance.
        /// </summary>
        public static EmptyState Get() {
            return _instance;
        }
    }
}
