﻿using UnityEngine;

namespace HarmonicUnity
{
    /// <summary>
    /// Abstract class for a state in a StateMachine.
    /// Extend this class to implement specific state logic.
    /// </summary>
    public abstract class StateMachineState
    {
        /// <summary>
        /// StateMachine that this state belongs to.
        /// </summary>
        public StateMachine StateMachine;

        /// <summary>
        /// GameObject of the StateMachine.
        /// </summary>
        protected GameObject _gameObject { get { return StateMachine.gameObject; } }

        protected float _startTime { get; private set; }

        protected float _startFixedTime { get; private set; }

        protected float _elaspedTime { get { return Time.time - _startTime; } }

        public float _elapsedFixedTime { get { return Time.fixedDeltaTime * _fixedFrameCount; } }

        protected int _fixedFrameCount { get; private set; }

        /// <summary>
        /// Called immediately after the state is switched to.
        /// </summary>
        public virtual void OnStart() {
            _startTime = Time.time;
            _startFixedTime = Time.fixedTime;
        }

        /// <summary>
        /// Called on each Update frame while the state is active.
        /// </summary>
        public virtual void OnUpdate() {
        }

        /// <summary>
        /// Called on each FixedUpdate frame while the state is active.
        /// </summary>
        public virtual void OnFixedUpdate() {
            ++_fixedFrameCount;
        }

        /// <summary>
        /// Called immediately before switching to another state.
        /// </summary>
        public virtual void OnEnd() {
        }

        protected void ChangeState(StateMachineState newState) {
            StateMachine.ChangeState(newState);
        }
    }
}
