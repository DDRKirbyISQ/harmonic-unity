﻿using UnityEngine;

namespace HarmonicUnity
{
    /// <summary>
    /// A simple, generic finite state machine implementation.
    /// </summary>
    public class StateMachine : MonoBehaviour
    {
        /// <summary>
        /// Current state of the state machine.
        /// </summary>
        public StateMachineState CurrentState { get; private set; }

#if UNITY_EDITOR
        public string StateName;
#endif

        /// <summary>
        /// Changes to a new state.
        /// </summary>
        /// <param name="newState">New state.</param>
        public void ChangeState(StateMachineState newState) {
            CurrentState.OnEnd();
            newState.StateMachine = this;
            CurrentState = newState;
            newState.OnStart();
        }

        protected void ConstructStateMachine() {
            CurrentState = EmptyState.Get();
        }

        protected virtual void Awake() {
            ConstructStateMachine();
        }

        public virtual void Update() {
            CurrentState.OnUpdate();

#if UNITY_EDITOR
            StateName = CurrentState.GetType().ToString();
#endif
        }

        public virtual void FixedUpdate() {
            CurrentState.OnFixedUpdate();
        }
    }
}
