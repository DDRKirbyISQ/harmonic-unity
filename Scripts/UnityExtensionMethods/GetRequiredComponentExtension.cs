﻿using UnityEngine;
using UnityEngine.Assertions;

namespace HarmonicUnity
{
    /// <summary>
    /// Defines convenience wrappers for retrieving a required component and raising an Assert if necessary.
    /// </summary>
    public static class GetRequiredComponentExtension
    {
        /// <summary>
        /// Retrieves a component from the GameObject, raising an Assert if it does not exist.
        /// </summary>
        /// <typeparam name="T">Type of component to retrieve.</typeparam>
        public static T GetRequiredComponent<T>(this GameObject gameObject) where T : Component {
            if (!gameObject.TryGetComponent<T>(out T result)) {
                Debug.LogErrorFormat("Missing required component {0} for {1}", typeof(T), gameObject);
            }
            return result;
        }

        /// <summary>
        /// Retrieves a component from the script's GameObject, raising an Assert if it does not exist.
        /// </summary>
        /// <typeparam name="T">Type of component to retrieve.</typeparam>
        public static T GetRequiredComponent<T>(this MonoBehaviour monoBehaviour) where T : Component {
            return monoBehaviour.gameObject.GetRequiredComponent<T>();
        }

        /// <summary>
        /// Retrieves a component from the transform's GameObject, raising an Assert if it does not exist.
        /// </summary>
        /// <typeparam name="T">Type of component to retrieve.</typeparam>
        public static T GetRequiredComponent<T>(this Transform transform) where T : Component {
            return transform.gameObject.GetRequiredComponent<T>();
        }

    }
}