﻿using UnityEngine;

namespace HarmonicUnity
{
    public static class Vector2Extension
    {
        /// <summary>
        /// Returns the angle of the vector in degrees, where 0.0f is Vector2.right and 90.0f is Vector2.up.
        /// </summary>
        /// <returns>The angle, in the range [0.0f, 360.0f). </returns>
        public static float Angle(this Vector2 vector) {
            float rads = Mathf.Atan2(vector.y, vector.x);
            float degrees = rads * Mathf.Rad2Deg;
            return Utils.ModPositive(degrees, 360.0f);
        }
    }
}

